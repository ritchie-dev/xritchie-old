---
lang: zh-CN
title: 抽象类
description: obj 04 抽象类
---

### 抽象方法abstract

- 没有实现的方法
- 没有方法体
- 有抽象方法的类，一定要声明成抽象类
- 抽象类不能被实例化

