---
lang: zh-CN
title: 运算符
description: se 08
---

## 编码
ascii不支持汉字 ---- unicode浪费空间 ---- utf-8‍

一个汉字 gbk使用2个字节，utf-8使用3个字节‍

## 运算符

算术，关系，逻辑，赋值，三元，二进制，位运算符

原码，反码，补码，计算机运算时都是以*补码*的方式运算的。

前++:   ++i先自增后赋值

后++:   i++先赋值再自增

逻辑与&

短路与&&，用的多，效率高

三元运算符


**instanceof二元运算符**

对象**运算符**(**instanceof**)用来判断一个对象是否属于某个指定的类或其子类的实例，如果是，返回真(true)，否则返回假(false)。‍




## 进制

十进制Decimal

十六进制Hexadecimal

八进制Octal

二进制Binary
