
java基础: 
    简介，环境配置，对象，类，数据类型，变量，修饰符，运算符，循环，条件，常用类，正则，异常，数组，常用方法

面向对象：
    继承，多态，抽象类，封装，接口，枚举，包

java中级：
    数据结构，网络编程，8新特性，io，集合，泛型，lambda，多线程

前端：
    html，css，js，json，ajax，jquery，bootstrap，vue，node.js，axios，less

javaee：
    jdbc，servlet，jsp，mvc，filter，listen，tomcat

javaee框架: 
    MyBatis，Spring，SpringBoot，Shiro，Spring Security

DevTools：
    Git，Maven，Redis，Nginx，Linux，Docker，搜索引擎

面试题，faq，游戏




### 杂七杂八
<p id="hitokoto">
  <a href="#" id="hitokoto_text">:D 获取中...</a>
</p>



VuePress 2 已经发布 :tada: ！
[[toc]]

:rainbow: 
:snowflake:
:snowman_with_snow:
:sparkles:
:blossom:小花
:star:
:fire:
:dart:
:octocat:
:trollface:

```ts{1,6-8}
这
里面
随便写点
ts代码
666
加油
哈哈
```



```ts
// 行号默认是启用的
const line2 = 'This is line 2'
const line3 = 'This is line 3'
```

```ts:no-line-numbers
// 行号被禁用
const line2 = 'This is line 2'
const line3 = 'This is line 3'
```

<!-- 最简单的语法 -->
@[code](../foo.js)

<!-- 仅导入第 1 行至第 10 行 -->
@[code{1-10}](../foo.js)

<!-- 指定代码语言 -->
@[code js](../foo.js)

<!-- 行高亮 -->
@[code js{2,4-5}](../foo.js)

<!-- ![VuePress Logo](/images/hero.png) -->
![VP Logo](/images/hero.png)

<!-- 相对路径 -->
[首页](../README.md)  
[配置参考](../reference/config.md)  
[快速上手](./getting-started.md)  
<!-- 绝对路径 -->
[指南](/zh/README.md)  
[配置参考 > markdown.links](/zh/reference/config.md#links)  
<!-- URL -->
[GitHub](https://github.com) 