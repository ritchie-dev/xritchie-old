import{_ as e,p as i,q as n,a1 as d}from"./framework-7db056f4.js";const l={},s=d(`<h3 id="javascript概述" tabindex="-1"><a class="header-anchor" href="#javascript概述" aria-hidden="true">#</a> JavaScript概述</h3><h4 id="js简介" tabindex="-1"><a class="header-anchor" href="#js简介" aria-hidden="true">#</a> JS简介</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>0. JavaScript是一门计算机语言(人与计算机交流的语言)
1. JavaScript(简称&quot;JS&quot;)一种直译式脚本语言,是一种动态类型、弱类型、基于原型的语言
2. JavaScript的作者 布兰登·艾克
3. 它的解释器被称为JavaScript引擎,为浏览器的一部分,是广泛用于客户端的脚本语言
4. 最早是在HTML网页上使用,用来给HTML网页增加动态功能
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="js应用场景" tabindex="-1"><a class="header-anchor" href="#js应用场景" aria-hidden="true">#</a> JS应用场景</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>简单应用: 按钮点击,浮动布局,幻灯片
复杂应用: 游戏、2D/3D动画、大型数据库驱动程序
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="js发展史" tabindex="-1"><a class="header-anchor" href="#js发展史" aria-hidden="true">#</a> JS发展史</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. JavaScript在1995年由Netscape公司的Brendan Eich,在网景导航者浏览器上首次设计实现而成
2. Netscape在最初将其脚本语言命名为LiveScript,后来Netscape与Sun合作,Netscape管理层希望它外观看起来像Java,因此取名为JavaScript
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="javascript的书写位置" tabindex="-1"><a class="header-anchor" href="#javascript的书写位置" aria-hidden="true">#</a> JavaScript的书写位置</h3><ul><li>行内式</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>JS写到HTML标签中

例如: &lt;button onclick=&quot;alert(&#39;行内式&#39;)&quot;&gt;行内式&lt;/button&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>内嵌式(开发中常见)</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>将JS代码写到HTML文件中的head标签中,使用方式和css类似,将JS代码写到script标签中

例如:
&lt;script type=&quot;text/javascript&quot;&gt;
	alert(&#39;内嵌式&#39;);
&lt;/script&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>外部式(开发中常见)</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>在外部创建一个以.js结尾的文件,然后在当前HTML文件中使用script标签引入

例如:
1. 在head标签中引入
2. 在body标签中引入
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="js入门之helloworld" tabindex="-1"><a class="header-anchor" href="#js入门之helloworld" aria-hidden="true">#</a> JS入门之HelloWorld</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>基本所有的计算机语言在学习之前都会编写一个入门的代码,输出HelloWorld字符串

点击button按钮,在浏览器弹窗HelloWorld

方式一(行内式)

&lt;button onclick=&quot;alert(&#39;HelloWorld&#39;)&quot;&gt;HelloWorld&lt;/button&gt;

方式二(内嵌式)

&lt;button id=&quot;btn&quot;&gt;HelloWorld&lt;/button&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
	window.onload=function(){
		var btn = document.getElementById(&quot;btn&quot;);
		btn.onclick=function(){
			alert(&quot;HelloWorld&quot;);
		}
	}
&lt;/script&gt;

方式三(外部式): 外部式代码和内嵌式代码一模一样,就是将JS代码写到外部的JS文件中.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="javascript基础语法" tabindex="-1"><a class="header-anchor" href="#javascript基础语法" aria-hidden="true">#</a> JavaScript基础语法</h3><h4 id="变量声明" tabindex="-1"><a class="header-anchor" href="#变量声明" aria-hidden="true">#</a> 变量声明</h4><ul><li>变量的概念</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 变量是存储值的容器
2. 内存中的一个存储单元
3. 该单元有自己的名称（变量名）和类型（数据类型）
4. 变量必须先声明,后使用
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>变量的声明</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>JS声明变量使用关键字 var 

关键字: 被JavaScript语言赋予了特殊含义,用作专门用途的字符串(单词)

例如: 
	var num=10;    var:声明变量的关键字  num: 给变量起的名字,名字自定义(见名知意,用英文,不允许用汉语拼音)
	var s=&quot;Hello&quot;;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="基本数据类型" tabindex="-1"><a class="header-anchor" href="#基本数据类型" aria-hidden="true">#</a> 基本数据类型</h4><ul><li>常见数据库类型介绍</li></ul><table><thead><tr><th>变量</th><th>解释</th><th>示例</th></tr></thead><tbody><tr><td>String</td><td>字符串: 一串文字,字符串的值必须用引号(单引号或者双引号)括起来</td><td>var s=&#39;李雷&#39;</td></tr><tr><td>Number</td><td>数字(整数/小数)</td><td>var x=10; var y=1.0;</td></tr><tr><td>Boolean</td><td>布尔值: 真或者假. 真:true;假:false</td><td></td></tr><tr><td>Array</td><td>数组: 存储一组数据</td><td>var arr=[1,2,&#39;Polly&#39;,[3,4,5]];</td></tr><tr><td>Object</td><td>对象: 万事万物皆对象,一切都可以保存到变量中</td><td>var obj1={}; var obj2 = new Object();</td></tr></tbody></table><ul><li>获取数据的数据类型以及注释</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 控制台输出函数 console.log(&#39;要输出的内容&#39;);
2. typeof(&quot;传入要查询的数据值的变量名&quot;);  typeof:是一个函数,返回数据的类型.


JavaScript的注释和CSS类型,JS分为两种注释:
1. 单行注释  //单行注释
2. 多行注释  /* 多行注释 */
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="对象" tabindex="-1"><a class="header-anchor" href="#对象" aria-hidden="true">#</a> 对象</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>JS是一门纯面向对象的语言:万事万物皆对象

JS创建对象的方式:
方式一:
var obj = {};
//赋值
obj.id=100;
obj.name=&quot;李雷&quot;;
//控制台输出
console.log(obj);
//获取属性值
console.log(obj.name);

方式二:
var obj = new Object();
//赋值
obj.id=100;
obj.name=&quot;李雷&quot;;
//控制台输出
console.log(obj);
//获取属性值
console.log(obj.name);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="数组" tabindex="-1"><a class="header-anchor" href="#数组" aria-hidden="true">#</a> 数组</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>创建数组的三种方式:
方式一:
var arr1=[&#39;AAA&#39;,123];
方式二:
var arr2=new Array(&#39;AAA&#39;,123);
方式三:
var arr3=new Array(10);
arr3[0]=1;
arr3[1]=&quot;Hello&quot;;
注意: 当new Array(10)构造器里面值赋一个值或者不赋值时,代表的是数组的长度,当大于一个值是表示数组的内容,构造器内输入数据的类型可以是任意类型


1.数组函数
1.1 push() : 向数组尾部添加一个元素
1.2 pop()  : 从数组尾部弹出一个元素,并且返回弹出的元素
1.3 shift(): 头部删除一个元素
1.4 unshift(): 头部添加一个元素
1.5 concat([数组]): 数组连接  [1,2,3].concat([4,5]) [1,2,3,4,5]
1.6 join(): [1,2,3].join(&quot;-&quot;) 1-2-3
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="运算符" tabindex="-1"><a class="header-anchor" href="#运算符" aria-hidden="true">#</a> 运算符</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>运算符: 
算数运算符: + - * / %  += -= *= /= %=  ++ --
关系运算符: &lt; &gt; &gt;= &lt;= == != ===(严格等于,既比较值又比较数据类型)
逻辑运算符: &amp;&amp; || !
三目运算符: 表达式 ? 结果1:结果2
语法规则和Java基本相同,在进行除法运算时,因为没有整数类型和浮点类型的区分所以,不会取整

注意: 虽然js是一个所类型语言,但是当遇到下面的运算时也需要类型转换.

var a=10;
var b=&quot;10&quot;;
var sum=a+b;//如果不将b进行类型转换,那么就会变成字符串拼接.

parseInt();将字符串转换成number

var sum=parseInt(b)+a;
console.log(sum);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="条件分支结构" tabindex="-1"><a class="header-anchor" href="#条件分支结构" aria-hidden="true">#</a> 条件分支结构</h4><ul><li>if语句</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>if(true/false){
    
}else if(true/false){
    
}else{
    
}

语法格式和Java语言基本相同
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>switch流程控制</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>var a=2;
switch(a){
  case 1:
	/* 浏览器输出 */
    document.write(&quot;值为1&quot;);
    break;
  case 2:
	/* 控制台输出 */
    console.log(&quot;值为2&quot;);
    break;
  case 3:
    document.write(&quot;值为3&quot;);
    break;
  default:
     document.write(&quot;值不是3也不是2也不是1&quot;);
}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="循环结构" tabindex="-1"><a class="header-anchor" href="#循环结构" aria-hidden="true">#</a> 循环结构</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>for循环
while循环

break与continue关键字
- break用于结束循环
- continue用于结束本次循环

/*
	打印九九乘法表
*/ 
for(var i=1;i&lt;=9;i++){
	for(j=1;j&lt;=i;j++){
		document.write(i+&quot;*&quot;+j+&quot;=&quot;+(i*j)+&quot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&quot;);
	}
	document.write(&quot;&lt;br&gt;&quot;);
}

语法和Java相同
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="函数" tabindex="-1"><a class="header-anchor" href="#函数" aria-hidden="true">#</a> 函数</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>函数又叫做方法,和我们数学中的方法概念相似,函数可以帮助我们完成特定功能 例如: sin()函数 cos()函数等
在计算机中: 函数就是实现特定功能的一段代码,可反复被调用
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>方法的声明</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>函数:也叫方法
js定义函数使用关键字function
方式一:
function 方法名(参数列表){方法体}
function sum(a,b,c,d){
    函数体
}
函数的返回值 return js函数的方法不用声明返回值类型

方式二:匿名方法
var sum=function(){}

tips: 万事万物皆对象,所以函数也是一个对象
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>方法的调用</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>//方式一
function sum(a,b,c){
	return a+b+c;
}
//方式二
var sum1 = function(a,b){
	return a+b;
}
//调用一
var s = sum(10,29,30);
console.log(s);
//调用二
var d = sum1(20,30);
console.log(d);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>将函数赋给对象</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>var obj = {};
//方式一
function sum(a,b,c){
	return a+b+c;
}
//方式二
var sum1 = function(a,b){
	return a+b;
}
//给对象赋值
obj.name=&quot;韩梅梅&quot;;
//将方法赋给对象
obj.fun1 = sum;
obj.fun2 = sum1;
console.log(obj);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>对象函数的调用</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>var obj = {};
//方式一
function sum(a,b,c){
	return a+b+c;
}
//方式二
var sum1 = function(a,b){
	return a+b;
}
//给对象赋值
obj.name=&quot;韩梅梅&quot;;
//将方法赋给对象
obj.fun1 = sum;
obj.fun2 = sum1;
console.log(obj);
//方法调用
var r = obj.fun1(1,2,3);
console.log(r);
var r2 = obj.fun2(4,5);
console.log(r2);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>JS的常见内置对象和函数</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>时间对象: 
var myDate = new Date();
console.log(myDate);
console.log(myDate.getFullYear());//年
console.log(myDate.getMonth()+1);//月
console.log(myDate.getDate());//日
console.log(myDate.getHours());//时
console.log(myDate.getMinutes());//分
console.log(myDate.getSeconds());//秒
console.log(myDate.getDay());//星期日是0
console.log(myDate.toLocaleDateString());//日期
console.log(myDate.toLocaleTimeString());//时间
console.log(myDate.valueOf());//毫秒


数学对象Math
console.log(Math.PI);
console.log(Math.abs(-1));//绝对值
console.log(Math.floor(4.99));//向下取整
console.log(Math.max(1,2,3,4));//最大值
console.log(Math.min(1,2,3,4));//最小值
console.log(Math.random());//0-1随机数
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="cookie技术" tabindex="-1"><a class="header-anchor" href="#cookie技术" aria-hidden="true">#</a> Cookie技术</h4><ul><li>cookie介绍</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>cookie英文单词饼干,小甜点的意思,在JS中他是一段可以保存不超过4KB的小型文本数据,保存在浏览器中
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ul><li>cookie数据结构</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>cookie是以key=value的形式存储

例如: username=tom
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>cookie怎么使用</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 向浏览器中保存数据: document.cookie=&quot;username=tom&quot;;
2. 从浏览器中获取cookie数据 var c = document.cookie;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>cookie的其他参数</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. expires 设置cookie的过期时间,时间格式必须为UTC 或 GMT 时间格式
2. path 设置cookie的存储路径默认为 /
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>设置和获取cookie案例</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 设置cookie

//创建cookie
//创建过期时间
var d = new Date();//当前时间
//根据当前时间向后推一天作为过期时间
d.setTime(d.getTime()+2*24*60*60*1000);//向后推2*24*60*60*1000毫秒(2天)
//创建cookie格式,设置数据,过期时间,以及保存位置
var cookie_str=&quot;username=tom1;expires=&quot;+d.toGMTString()+&quot;;path=/&quot;;
document.cookie=cookie_str;
console.log(cookieStr);

2. 获取cookie

//获取cookie
var c = document.cookie;
console.log(c);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="正则表达式" tabindex="-1"><a class="header-anchor" href="#正则表达式" aria-hidden="true">#</a> 正则表达式</h3><h4 id="正则表达式介绍" tabindex="-1"><a class="header-anchor" href="#正则表达式介绍" aria-hidden="true">#</a> 正则表达式介绍</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 正则表达式是由一个字符序列形成的搜索模式
2. 当你在文本中搜索数据时，你可以用搜索模式来描述你要查询的内容
3. 正则表达式可以是一个简单的字符，或一个更复杂的模式
4. 正则表达式可用于所有文本搜索和文本替换的操作
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="正则表达式的语法" tabindex="-1"><a class="header-anchor" href="#正则表达式的语法" aria-hidden="true">#</a> 正则表达式的语法</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>/正则表达式主体/修饰符(可选)
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h4 id="js中支持正则表达式的常见对象和方法" tabindex="-1"><a class="header-anchor" href="#js中支持正则表达式的常见对象和方法" aria-hidden="true">#</a> JS中支持正则表达式的常见对象和方法</h4><ul><li>RegExp对象</li></ul><table><thead><tr><th>方法</th><th>描述</th></tr></thead><tbody><tr><td>exec</td><td>检索字符串中指定的值。返回找到的值,并确定其位置</td></tr><tr><td>test</td><td>检索字符串中指定的值。返回 true 或 false</td></tr></tbody></table><ul><li>String对象</li></ul><table><thead><tr><th>方法</th><th>描述</th></tr></thead><tbody><tr><td>search</td><td>检索与正则表达式相匹配的值</td></tr><tr><td>match</td><td>找到一个或多个正则表达式的匹配</td></tr><tr><td>replace</td><td>替换与正则表达式匹配的子串</td></tr><tr><td>split</td><td>把字符串分割为字符串数组</td></tr></tbody></table><h4 id="正则表达式的使用" tabindex="-1"><a class="header-anchor" href="#正则表达式的使用" aria-hidden="true">#</a> 正则表达式的使用</h4><ul><li>构建正则表达式的特殊字符</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>修饰符: 用于执行区分大小写和全局匹配
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><table><thead><tr><th>修饰符</th><th>描述</th></tr></thead><tbody><tr><td>i</td><td>执行对大小写不敏感的匹配</td></tr><tr><td>g</td><td>执行全局匹配（查找所有匹配而非在找到第一个匹配后停止）</td></tr><tr><td>m</td><td>执行多行匹配</td></tr></tbody></table><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>方括号: 用于查找某个范围内的字符
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><table><thead><tr><th>表达式</th><th>描述</th></tr></thead><tbody><tr><td>[abc]</td><td>查找方括号之间的任何字符</td></tr><tr><td>[^abc]</td><td>查找任何不在方括号之间的字符</td></tr><tr><td>[0-9]</td><td>查找任何从 0 至 9 的数字</td></tr><tr><td>[a-z]</td><td>查找任何从小写 a 到小写 z 的字符</td></tr><tr><td>[A-Z]</td><td>查找任何从大写 A 到大写 Z 的字符</td></tr><tr><td>[A-z]</td><td>查找任何从大写 A 到小写 z 的字符</td></tr><tr><td>(red</td><td>blue</td></tr></tbody></table><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>元字符(Metacharacter): 是拥有特殊含义的字符
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><table><thead><tr><th>元字符</th><th>描述</th></tr></thead><tbody><tr><td>.</td><td>查找单个字符,除了换行和行结束符</td></tr><tr><td>\\w</td><td>查找单词字符</td></tr><tr><td>\\W</td><td>查找非单词字符</td></tr><tr><td>\\d</td><td>查找数字</td></tr><tr><td>\\D</td><td>查找非数字字符</td></tr><tr><td>\\s</td><td>查找空白字符</td></tr><tr><td>\\S</td><td>查找非空白字符</td></tr><tr><td>\\b</td><td>匹配单词边界</td></tr><tr><td>\\B</td><td>匹配非单词边界</td></tr><tr><td>\\0</td><td>查找 NULL 字符</td></tr><tr><td>\\n</td><td>查找换行符</td></tr><tr><td>\\f</td><td>查找换页符</td></tr><tr><td>\\r</td><td>查找回车符</td></tr><tr><td>\\t</td><td>查找制表符</td></tr><tr><td>\\v</td><td>查找垂直制表符</td></tr><tr><td>\\xxx</td><td>查找以八进制数 xxx 规定的字符</td></tr><tr><td>\\xdd</td><td>查找以十六进制数 dd 规定的字符</td></tr><tr><td>\\uxxxx</td><td>查找以十六进制数 xxxx 规定的 Unicode 字符</td></tr></tbody></table><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>量词: 用于表示重复次数的含义
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><table><thead><tr><th>量词</th><th>描述</th></tr></thead><tbody><tr><td>n+</td><td>匹配任何包含至少一个 n 的字符串</td></tr><tr><td>n*</td><td>匹配任何包含零个或多个 n 的字符串</td></tr><tr><td>n?</td><td>匹配任何包含零个或一个 n 的字符串</td></tr><tr><td>n{X}</td><td>匹配包含 X 个 n 的序列的字符串</td></tr><tr><td>n{X,}</td><td>X 是一个正整数。前面的模式 n 连续出现至少 X 次时匹配</td></tr><tr><td>n{X,Y}</td><td>X 和 Y 为正整数。前面的模式 n 连续出现至少 X 次，至多 Y 次时匹配</td></tr><tr><td>n$</td><td>匹配任何结尾为 n 的字符串</td></tr><tr><td>^n</td><td>匹配任何开头为 n 的字符串</td></tr><tr><td>?=n</td><td>匹配任何其后紧接指定字符串 n 的字符串</td></tr><tr><td>?!n</td><td>匹配任何其后没有紧接指定字符串 n 的字符串</td></tr></tbody></table><ul><li>常见正则表达式的构建</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 匹配手机号

var phone=/^[1][3,4,5,7,8][0-9]{9}$/;

^: 匹配输入的开始
[1]: 第1位以1开始
[3,4,5,7,8] : 第2位可以是3,4,5,7,8  这时候手机号前两位就出来了 13,14,15,17,18 如果以后手机号还有别的开头的比如说19,那么在第二个中括号中添加9即可
[0-9]{9}$ : {9}表示前面还有9位可以是0-9任意; $: 表示结尾

2. 匹配身份证

//校验身份证15位或者18位的正则,如果为18位后一位可能是数字或者x
var cardId=/(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)/;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>正则表达式的使用</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 检索字符串中某一个子串是否存在

//使用string对象中的方法
var str=&quot;abCDef&quot;;
//返回cd所在字符串中的下标值,如果不存在返回-1  i: 表示不区分大小写,可以省略
var f = str.search(/cd/i);
console.log(f);

2. 检索一个字符串是不是以数字开头

//使用JavaScript对象
var reg=/^\\d/; //以数字开头
var f = reg.test(&quot;1abc&quot;);//如果以数字开头返回true
console.log(f);

3. 检索一个字符串是不是正确的手机号

var phone=/^[1][3,4,5,7,8][0-9]{9}$/;
var f = phone.test(&quot;19252565897&quot;);
console.log(f);

4. 检索一个字符串是不是一个正确的身份证号

var cardId=/(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)/;
var f = cardId.test(&quot;23230319966582256x&quot;);
console.log(f);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="html-dom-文档对象模型" tabindex="-1"><a class="header-anchor" href="#html-dom-文档对象模型" aria-hidden="true">#</a> HTML DOM(文档对象模型)</h3><h4 id="dom介绍" tabindex="-1"><a class="header-anchor" href="#dom介绍" aria-hidden="true">#</a> DOM介绍</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 当网页被加载时,浏览器会创建页面的文档对象模型DOM(Document Object Model),将整个.html文件读成一个对象(document对象)
2. DOM是一项W3C(World Wide Web Consortium)标准,它允许程序和脚本动态地访问、更新文档的内容、结构和样式
3. W3C: 万维网联盟,又称W3C理事会,是国际最著名的标准化组织,W3C最重要的工作是制定、发展 Web规范
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="dom树" tabindex="-1"><a class="header-anchor" href="#dom树" aria-hidden="true">#</a> DOM树</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>DOM树: document对象里描述了整个.html文件(包含各个标签元素),里面的各个元素之间产生了各种关系,这种关系就像树形结构

document对象由浏览器创建,在浏览器加载.html文件后生成,描述了整个DOM树对象,各个标签元素存在于docuemnt对象中,如果想获取元素标签对象可以从docuemnt对象中查找
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><img src="https://note.youdao.com/yws/api/personal/file/WEBb7d88519df07d390b933a461aa3ee97f?method=download&amp;shareKey=9552e94a6ef8323f3c22e7e97a0eb25d"><h4 id="获取元素对象的方式" tabindex="-1"><a class="header-anchor" href="#获取元素对象的方式" aria-hidden="true">#</a> 获取元素对象的方式</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>通过document对象调用
一、获取元素节点:
1. getElementById(): 通过id属性获取一个元素节点对象
2. getElementsByTagName(): 通过标签名获取一组元素节点对象
3. document.getElementsByClassName():通过类名获取一组元素节点对象
4. getElementsByName(): 通过name属性获取一组元素节点对象

二、获取元素节点的子节点
1. getElementsByTagName(): 方法，返回当前节点的指定标签名后代节点
2. childNodes: 属性，表示当前节点的所有子节点
3. firstChild: 属性，表示当前节点的第一个子节点
4. lastChild: 属性，表示当前节点的最后一个子节点

三、获取父节点和兄弟节点
1. parentNode: 属性，表示当前节点的父节点
2. previousSibling: 属性，表示当前节点的前一个兄弟节点
3. nextSibling: 属性，表示当前节点的后一个兄弟节点
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="元素节点对象获取" tabindex="-1"><a class="header-anchor" href="#元素节点对象获取" aria-hidden="true">#</a> 元素节点对象获取</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>节点: Node——构成HTML文档最基本的单元
常用节点分为四类:
    – 文档节点:整个HTML文档
    – 元素节点:HTML文档中的HTML标签 
    – 属性节点:元素的属性
    – 文本节点:HTML标签中的文本内容
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><img src="https://note.youdao.com/yws/api/personal/file/WEBbd6782fb7a7b80b8efe32f11d0f57010?method=download&amp;shareKey=753112247d53a9600488a0a5e14a7ca9"><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1、文档节点(document)
   - 1.1 文档节点document,代表的是整个HTML文档,网页中的所有节点都是它的子节点
   - 1.2 document对象作为window对象的属性存在,我们不用获取可以直接使用.
   - 1.3 通过该对象我们可以在整个文档中查找节点对象,并可以通过该对象创建各种节点对象
2、元素节点(Element)
   - 2.1 HTML中的各种标签都是元素节点,这是我们最常用的一个节点
   - 2.2 浏览器会将页面中所有的标签都转换为一个元素节点,我们可以通过document的方法来获取元素节点
        -- 比如: 
        //JS代码,通过id获取元素节点
        var pEle = document.getElementById(&quot;pId&quot;);
		console.log(pEle);
		&lt;!--HTML代码--&gt;
		&lt;p id=&quot;pId&quot;&gt;这是一个P标签&lt;/p&gt;
   
3、文本节点(Text)
   - 3.1 文本节点表示的是HTML标签以外的文本内容,任意非HTML的文本都是文本节点
   - 3.2 它包括可以字面解释的纯文本内容
   - 3.3 文本节点一般是作为元素节点的子节点存在的
   - 3.4 获取文本节点时,一般先要获取元素节点.在通过元素节点获取文本节点
        -- 例如:
        //JS代码,通过id获取元素节点
        var pEle = document.getElementById(&quot;pId&quot;);
		console.log(pEle);
		//获取文本节点
		var textEle = pEle.firstChild;
		console.log(textEle);
		
		&lt;!--HTML代码--&gt;
		&lt;p id=&quot;pId&quot;&gt;这是一个P标签&lt;/p&gt;
        
4、属性节点(Attr)
   - 4.1 属性节点表示的是标签中的一个一个的属性，这里要注意的是属性节点并非是元素 节点的子节点，而是元素节点的一部分
   - 4.2 可以通过元素节点来获取指定的属性节点
        -- 例如:
        //JS代码,通过id获取元素节点
        var pEle = document.getElementById(&quot;pId&quot;);
		console.log(pEle);
		//获取属性节点
		var attrEle = pEle.getAttributeNode(&quot;id&quot;);
		console.log(attrEle);
		
		&lt;!--HTML代码--&gt;
		&lt;p id=&quot;pId&quot;&gt;这是一个P标签&lt;/p&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="元素节点内容的获取" tabindex="-1"><a class="header-anchor" href="#元素节点内容的获取" aria-hidden="true">#</a> 元素节点内容的获取</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>上面获取到了元素节点对象,接下来我们要从节点对象中获取元素节点的内容.

节点对象中包含各个属性,我们通过对象属性获取节点内容,属性如下:
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;table&gt;
	&lt;tr&gt;
		&lt;th&gt;&lt;/th&gt;
		&lt;th&gt;nodeName&lt;/th&gt;
		&lt;th&gt;nodeType&lt;/th&gt;
		&lt;th&gt;nodeValue&lt;/th&gt;
	&lt;/tr&gt;
	&lt;tr&gt;
		&lt;th&gt;文档节点&lt;/th&gt;
		&lt;td&gt;#document&lt;/td&gt;
		&lt;td&gt;9&lt;/td&gt;
		&lt;td&gt;null&lt;/td&gt;
	&lt;/tr&gt;
	&lt;tr&gt;
		&lt;th&gt;元素节点&lt;/th&gt;
		&lt;td&gt;标签名&lt;/td&gt;
		&lt;td&gt;1&lt;/td&gt;
		&lt;td&gt;null&lt;/td&gt;
	&lt;/tr&gt;
	&lt;tr&gt;
		&lt;th&gt;属性节点&lt;/th&gt;
		&lt;td&gt;属性名&lt;/td&gt;
		&lt;td&gt;2&lt;/td&gt;
		&lt;td&gt;属性值&lt;/td&gt;
	&lt;/tr&gt;
	&lt;tr&gt;
		&lt;th&gt;文本节点&lt;/th&gt;
		&lt;td&gt;#text&lt;/td&gt;
		&lt;td&gt;3&lt;/td&gt;
		&lt;td&gt;
			&lt;strong style=&quot;color:red;&quot;&gt;文本内容&lt;/strong&gt;
		&lt;/td&gt;
	&lt;/tr&gt;
&lt;/table&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>文档节点</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>//节点名称
var name = document.nodeName;
console.log(name);
//节点类型
var type = document.nodeType;
console.log(type);
//节点值
var value = document.nodeValue;
console.log(value);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>元素节点</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>//获取元素节点
var pEle = document.getElementById(&quot;pId&quot;);
//节点名称
var name = pEle.nodeName;
console.log(name);
//节点类型
var type = pEle.nodeType;
console.log(type);
//节点值
var value = pEle.nodeValue;
console.log(value);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>文本节点</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>//获取元素节点
var pEle = document.getElementById(&quot;pId&quot;);
//获取文本节点
var textEle = pEle.firstChild;
//节点名称
var name = textEle.nodeName;
console.log(name);
//节点类型
var type = textEle.nodeType;
console.log(type);
//节点值
var value = textEle.nodeValue;
console.log(value);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>属性节点</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>//获取元素节点
var pEle = document.getElementById(&quot;pId&quot;);
//获取属性节点
var attrEle = pEle.getAttributeNode(&quot;id&quot;);
console.log(attrEle);
//节点名称
var name = attrEle.nodeName;
console.log(name);
//节点类型
var type = attrEle.nodeType;
console.log(type);
//节点值
var value = attrEle.nodeValue;
console.log(value);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="元素节点的属性" tabindex="-1"><a class="header-anchor" href="#元素节点的属性" aria-hidden="true">#</a> 元素节点的属性</h4><ul><li>给元素节点的属性赋值</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;p id=&quot;pId&quot; title=&quot;标题&quot; mn=&quot;abc&quot; class=&quot;def&quot;&gt;这是一个P标签&lt;/p&gt;

//获取元素节点对象
var pEle = document.getElementById(&quot;pId&quot;);

console.log(pEle.id) //通过属性获取值
console.log(pEle.title) 
console.log(pEle.mn)  //mn属性不是p标签固有属性,是否能获取到值
console.log(pEle.className) //获取类(class)属性,使用className属性获取

//修改属性值
pEle.className=&quot;李雷&quot;;
console.log(pEle.className);
//给一个当前标签不存在的属性赋值
pEle.className1=&quot;李雷1&quot;;
console.log(pEle.className1);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>添加和获取元素的属性(setAttribute和getAttribute方法)</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;p id=&quot;pId&quot;&gt;这是一个P标签&lt;/p&gt;

var pEle = document.getElementById(&quot;pId&quot;);
//通过方法设置属性值
pEle.setAttribute(&quot;title&quot;,&quot;中国&quot;);
//通过方法获取属性值
var v = pEle.getAttribute(&quot;title&quot;);
console.log(v);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="元素节点创建以及元素节点内容的修改" tabindex="-1"><a class="header-anchor" href="#元素节点创建以及元素节点内容的修改" aria-hidden="true">#</a> 元素节点创建以及元素节点内容的修改</h4><ul><li>元素节点的创建和修改</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1、创建元素节点
    document.createElement(&quot;标签名&quot;);
2、创建文本节点
    document.createTextNode(&quot;北京&quot;);
3、删除节点
    父节点.removeChild(子节点);
4、替换节点
    父节点.replaceChild(新节点,旧节点);
5、插入节点
   5.1 父节点.appendChild(子节点);
   5.2 父节点.insertBefore(新节点,旧节点);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>设置/获取元素内容</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>innerText:设置元素内容,原样输出
innerHTML:设置元素内容,如果设置的是一个标签元素会被浏览器解析

HTML:
&lt;p id=&quot;pId&quot;&gt;ppppp&lt;/p&gt;

js:
var pIdEle = document.getElementById(&quot;pId&quot;);
pIdEle.innerHTML=&quot;&lt;h1&gt;看这里,我变~~~&lt;/h1&gt;&quot;;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="dom事件" tabindex="-1"><a class="header-anchor" href="#dom事件" aria-hidden="true">#</a> DOM事件</h4><ul><li>事件简介</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>事件:文档或浏览器窗口中发生的一些特定的交互瞬间.
JavaScript 与 HTML 之间的交互是通过事件实现的.

事件三要素:
事件源: 被触发的物体
事件类型: 用户的行为,单击，双击，移入，移出，拖拽，滑屏
事件处理函数: 执行的代码
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>事件类型</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>单击 onclick
移入 onmouseover
移出 onmouseout
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>事件绑定的方式</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 方式一

HTML:
&lt;button id=&quot;btn&quot; &gt;JavaScript事件绑定&lt;/button&gt;

JS:
var btn = document.getElementById(&quot;btn&quot;);
btn.onclick=function(){
	alert(&quot;+++++++&quot;);
}

2. 方式二

HTML:
&lt;button id=&quot;btn&quot; &gt;JavaScript事件绑定&lt;/button&gt;

JS:
var btn = document.getElementById(&quot;btn&quot;);
// click:不带on的事件类型
// function:事件处理函数
// false:指定事件是否在捕获或冒泡阶段执行
btn.addEventListener(&quot;click&quot;,function(){
	alert(&quot;=====&quot;);
},false);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>定时器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>//设置定时器1:时间到了之后定时器执行一次然后结束
setTimeout(&quot;参数1&quot;,&quot;参数2&quot;);
参数1: 被定时的函数
参数2: 定时时间(多久执行一次函数 ms)
eg: setTimeout(function(){console.log(&quot;=====&quot;)},3000);

//设置定时器2:根据时间循环执行函数
setInterval(&quot;参数1&quot;,&quot;参数2&quot;);
参数1: 被定时的函数
参数2: 定时时间(多久执行一次函数 ms)
var timeId = setInterval(function(){console.log(&quot;=====&quot;)},3000);

//取消定时器
//可以指定要取消的哪个定时器timeId,为开启的定时器的返回值
window.clearInterval(timeId);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="js页面跳转" tabindex="-1"><a class="header-anchor" href="#js页面跳转" aria-hidden="true">#</a> JS页面跳转</h4><ul><li>JS跳转页面</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>window.location.href=&quot;https://www.baidu.com&quot;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ul><li>JS跳转目标页面参数获取</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>var url = location.href;
console.log(&quot;地址:&quot;,url)
var parameter = url.substring(url.lastIndexOf(&quot;?&quot;)+1,url.length)
console.log(&quot;parameter:&quot;,parameter)
var pars = parameter.split(&quot;&amp;&quot;);
console.log(&quot;pars:&quot;,pars);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="js设置元素的css样式" tabindex="-1"><a class="header-anchor" href="#js设置元素的css样式" aria-hidden="true">#</a> JS设置元素的CSS样式</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>使用JS设置元素的CSS样式
方式一:
HTML:
&lt;p id=&quot;pId&quot;&gt;ppppp&lt;/p&gt;
JS:
var pId = document.getElementById(&quot;pId&quot;);
pId.onclick=function(){
	pId.style.color=&quot;red&quot;;
}

方式二:
HTML:
&lt;p id=&quot;pId&quot;&gt;ppppp&lt;/p&gt;

JS:
var pId = document.getElementById(&quot;pId&quot;);
pId.onclick=function(){
	pId.style.cssText=&quot;color:red;font-size: 30px;&quot;;
}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,136),t=[s];function a(v,r){return i(),n("div",null,t)}const c=e(l,[["render",a],["__file","03js.html.vue"]]);export{c as default};
