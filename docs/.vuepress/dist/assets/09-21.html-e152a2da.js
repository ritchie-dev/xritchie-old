import{_ as e,p as i,q as d,a1 as n}from"./framework-7db056f4.js";const s={},a=n(`<h2 id="基础折线图" tabindex="-1"><a class="header-anchor" href="#基础折线图" aria-hidden="true">#</a> 基础折线图</h2><p>折线图主要用来展示数据项随着时间推移的趋势或变化。</p><h3 id="最简单的折线图" tabindex="-1"><a class="header-anchor" href="#最简单的折线图" aria-hidden="true">#</a> 最简单的折线图</h3><p>如果我们想建立一个横坐标是类目型（category）、纵坐标是数值型（value）的折线图，我们可以使用这样的方式：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    type: &#39;category&#39;,
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;]
  },
  yAxis: {
    type: &#39;value&#39;
  },
  series: [
    {
      data: [120, 200, 150],
      type: &#39;line&#39;
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在这个例子中，我们通过 <code>xAxis</code> 将横坐标设为类目型，并指定了对应的值；通过 <code>type</code> 将 <code>yAxis</code> 的类型设定为数值型。在 <code>series</code> 中，我们将系列类型设为 <code>line</code>，并且通过 <code>data</code> 指定了折线图三个点的取值。这样，就能得到一个最简单的折线图了。</p><blockquote><p>这里 <code>xAxis</code> 和 <code>yAxis</code> 的 <code>type</code> 属性都可以隐去不写。因为坐标轴的默认类型是数值型，而 <code>xAxis</code> 指定了类目型的 <code>data</code>，所以 <code>ECharts</code> 也能识别出这是类目型的坐标轴。为了让大家更容易理解，我们特意写了<code>type</code>。在实际的应用中，如果是 <code>&#39;value&#39;</code> 类型，也可以省略不写。</p></blockquote><h3 id="笛卡尔坐标系中的折线图" tabindex="-1"><a class="header-anchor" href="#笛卡尔坐标系中的折线图" aria-hidden="true">#</a> 笛卡尔坐标系中的折线图</h3><p>如果我们希望折线图在横坐标和纵坐标上都是连续的，即在笛卡尔坐标系中，应该如何实现呢？答案也很简单，只要把 <code>series</code> 的 <code>data</code> 每个数据用一个包含两个元素的数组表示就行了。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {},
  yAxis: {},
  series: [
    {
      data: [
        [20, 120],
        [50, 200],
        [40, 50]
      ],
      type: &#39;line&#39;
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="折线图样式设置" tabindex="-1"><a class="header-anchor" href="#折线图样式设置" aria-hidden="true">#</a> 折线图样式设置</h3><h4 id="折线的样式" tabindex="-1"><a class="header-anchor" href="#折线的样式" aria-hidden="true">#</a> 折线的样式</h4><p>折线图中折线的样式可以通过 <code>lineStyle</code> 设置。可以为其指定颜色、线宽、折线类型、阴影、不透明度等等，具体的可以参考配置项手册 <code>series.lineStyle</code> 了解。这里，我们以设置颜色（color）、线宽（width）和折线类型（type）为例说明。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;]
  },
  yAxis: {},
  series: [
    {
      data: [10, 22, 28, 23, 19],
      type: &#39;line&#39;,
      lineStyle: {
        normal: {
          color: &#39;green&#39;,
          width: 4,
          type: &#39;dashed&#39;
        }
      }
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>这里设置折线宽度时，数据点描边的宽度是不会跟着改变的，而应该在数据点的配置项中另外设置。</p><h4 id="数据点的样式" tabindex="-1"><a class="header-anchor" href="#数据点的样式" aria-hidden="true">#</a> 数据点的样式</h4><p>数据点的样式可以通过 <code>series.itemStyle</code> 指定填充颜色（color）、描边颜色（borderColor）、描边宽度（borderWidth）、描边类型（borderType）、阴影（shadowColor）、不透明度（opacity）等。与折线样式的设置十分相似，这里不再展开说明。</p><h3 id="在数据点处显示数值" tabindex="-1"><a class="header-anchor" href="#在数据点处显示数值" aria-hidden="true">#</a> 在数据点处显示数值</h3><p>在系列中，这数据点的标签通过 <code>series.label</code> 属性指定。如果将 <code>label</code> 下的 <code>show</code> 指定为<code>true</code>，则表示该数值默认时就显示；如果为 <code>false</code>，而 <code>series.emphasis.label.show</code> 为 <code>true</code>，则表示只有在鼠标移动到该数据时，才显示数值。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;]
  },
  yAxis: {},
  series: [
    {
      data: [10, 22, 28, 23, 19],
      type: &#39;line&#39;,
      label: {
        show: true,
        position: &#39;bottom&#39;,
        textStyle: {
          fontSize: 20
        }
      }
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="空数据" tabindex="-1"><a class="header-anchor" href="#空数据" aria-hidden="true">#</a> 空数据</h3><p>在一个系列中，可能一个横坐标对应的取值是“空”的，将其设为 0 有时并不能满足我们的期望--空数据不应被其左右的数据连接。</p><p>在 ECharts 中，我们使用字符串 <code>&#39;-&#39;</code> 表示空数据，这对其他系列的数据也是适用的。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;]
  },
  yAxis: {},
  series: [
    {
      data: [0, 22, &#39;-&#39;, 23, 19],
      type: &#39;line&#39;
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,24),l=[a];function c(r,v){return i(),d("div",null,l)}const o=e(s,[["render",c],["__file","09-21.html.vue"]]);export{o as default};
