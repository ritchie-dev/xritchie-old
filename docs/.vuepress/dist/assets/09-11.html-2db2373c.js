import{_ as e,p as i,q as d,a1 as n}from"./framework-7db056f4.js";const s={},a=n(`<h2 id="基本柱状图" tabindex="-1"><a class="header-anchor" href="#基本柱状图" aria-hidden="true">#</a> 基本柱状图</h2><p>柱状图（或称条形图）是一种通过柱形的长度来表现数据大小的一种常用图表类型。</p><p>设置柱状图的方式，是将 <code>series</code> 的 <code>type</code> 设为 <code>bar</code>。</p><h3 id="最简单的柱状图" tabindex="-1"><a class="header-anchor" href="#最简单的柱状图" aria-hidden="true">#</a> 最简单的柱状图</h3><p>最简单的柱状图可以这样设置：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;Mon&#39;, &#39;Tue&#39;, &#39;Wed&#39;, &#39;Thu&#39;, &#39;Fri&#39;, &#39;Sat&#39;, &#39;Sun&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;bar&#39;,
      data: [23, 24, 18, 25, 27, 28, 25]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在这个例子中，横坐标是类目型的，因此需要在 <code>xAxis</code> 中指定对应的值；而纵坐标是数值型的，可以根据 <code>series</code> 中的 <code>data</code>，自动生成对应的坐标范围。</p><h3 id="多系列的柱状图" tabindex="-1"><a class="header-anchor" href="#多系列的柱状图" aria-hidden="true">#</a> 多系列的柱状图</h3><p>我们可以用一个系列表示一组相关的数据，如果需要实现多系列的柱状图，只需要在 series 多添加一项就可以了— —</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;Mon&#39;, &#39;Tue&#39;, &#39;Wed&#39;, &#39;Thu&#39;, &#39;Fri&#39;, &#39;Sat&#39;, &#39;Sun&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;bar&#39;,
      data: [23, 24, 18, 25, 27, 28, 25]
    },
    {
      type: &#39;bar&#39;,
      data: [26, 24, 18, 22, 23, 20, 27]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="柱状图样式设置" tabindex="-1"><a class="header-anchor" href="#柱状图样式设置" aria-hidden="true">#</a> 柱状图样式设置</h3><h4 id="柱条样式" tabindex="-1"><a class="header-anchor" href="#柱条样式" aria-hidden="true">#</a> 柱条样式</h4><p>柱条的样式可以通过 series.itemStyle 设置，包括：</p><ul><li>柱条的颜色（color）；</li><li>柱条的描边颜色（borderColor）、宽度（borderWidth）、样式（borderType）；</li><li>柱条圆角的半径（barBorderRadius）；</li><li>柱条透明度（opacity）；</li><li>阴影（shadowBlur、shadowColor、shadowOffsetX、shadowOffsetY）。</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;bar&#39;,
      data: [
        10,
        22,
        28,
        {
          value: 43,
          // 设置单个柱子的样式
          itemStyle: {
            color: &#39;#91cc75&#39;,
            shadowColor: &#39;#91cc75&#39;,
            borderType: &#39;dashed&#39;,
            opacity: 0.5
          }
        },
        49
      ],
      itemStyle: {
        barBorderRadius: 5,
        borderWidth: 1,
        borderType: &#39;solid&#39;,
        borderColor: &#39;#73c0de&#39;,
        shadowColor: &#39;#5470c6&#39;,
        shadowBlur: 3
      }
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在这个例子中，我们通过设置柱状图对应 <code>series</code> 的<code>itemStyle</code>，设置了柱条的样式。完整的配置项及其用法请参见配置项手册 <code>series.itemStyle</code>。</p><h4 id="柱条宽度和高度" tabindex="-1"><a class="header-anchor" href="#柱条宽度和高度" aria-hidden="true">#</a> 柱条宽度和高度</h4><p>柱条宽度可以通过 <code>barWidth</code> 设置。比如在下面的例子中，将 <code>barWidth</code> 设为 <code>&#39;20%&#39;</code>，表示每个柱条的宽度就是类目宽度的 20%。由于这个例子中，每个系列有 5 个数据，20% 的类目宽度也就是整个 x 轴宽度的 4%。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;bar&#39;,
      data: [10, 22, 28, 43, 49],
      barWidth: &#39;20%&#39;
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>另外，还可以设置 <code>barMaxWidth</code> 限制柱条的最大宽度。对于一些特别小的数据，我们也可以为柱条指定最小高度 <code>barMinHeight</code>，当数据对应的柱条高度小于该值时，柱条高度将采用这个最小高度。</p><h4 id="柱条间距" tabindex="-1"><a class="header-anchor" href="#柱条间距" aria-hidden="true">#</a> 柱条间距</h4><p>柱条间距分为两种，一种是不同系列在同一类目下的距离 <code>barGap</code>，另一种是类目与类目的距离 <code>barCategoryGap</code>。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;bar&#39;,
      data: [23, 24, 18, 25, 18],
      barGap: &#39;20%&#39;,
      barCategoryGap: &#39;40%&#39;
    },
    {
      type: &#39;bar&#39;,
      data: [12, 14, 9, 9, 11]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在这个例子中，<code>barGap</code> 被设为 <code>20%&#39;</code>，这意味着每个类目（比如 A）下的两个柱子之间的距离，相对于柱条宽度的百分比。而 <code>barCategoryGap</code> 是 <code>&#39;40%&#39;</code>，意味着柱条每侧空余的距离，相对于柱条宽度的百分比。</p><p>通常而言，设置 <code>barGap</code> 及 <code>barCategoryGap</code> 后，就不需要设置 <code>barWidth</code> 了，这时候的宽度会自动调整。如果有需要的话，可以设置 <code>barMaxWidth</code> 作为柱条宽度的上限，当图表宽度很大的时候，柱条宽度也不会太宽。</p><blockquote><p>在同一坐标系上，此属性会被多个柱状图系列共享。此属性应设置于此坐标系中最后一个柱状图系列上才会生效，并且是对此坐标系中所有柱状图系列生效。</p></blockquote><h4 id="为柱条添加背景色" tabindex="-1"><a class="header-anchor" href="#为柱条添加背景色" aria-hidden="true">#</a> 为柱条添加背景色</h4><p>有时，我们希望能够为柱条添加背景色。从 ECharts 4.7.0 版本开始，这一功能可以简单地用 <code>showBackground</code> 开启，并且可以通过 <code>backgroundStyle</code> 配置。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    type: &#39;category&#39;,
    data: [&#39;Mon&#39;, &#39;Tue&#39;, &#39;Wed&#39;, &#39;Thu&#39;, &#39;Fri&#39;, &#39;Sat&#39;, &#39;Sun&#39;]
  },
  yAxis: {
    type: &#39;value&#39;
  },
  series: [
    {
      data: [120, 200, 150, 80, 70, 110, 130],
      type: &#39;bar&#39;,
      showBackground: true,
      backgroundStyle: {
        color: &#39;rgba(220, 220, 220, 0.8)&#39;
      }
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,29),l=[a];function r(v,c){return i(),d("div",null,l)}const b=e(s,[["render",r],["__file","09-11.html.vue"]]);export{b as default};
