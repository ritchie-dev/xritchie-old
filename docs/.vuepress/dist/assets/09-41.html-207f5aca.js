import{_ as e,p as i,q as n,a1 as d}from"./framework-7db056f4.js";const s={},a=d(`<h2 id="基础散点图" tabindex="-1"><a class="header-anchor" href="#基础散点图" aria-hidden="true">#</a> 基础散点图</h2><p>散点图，也是一种常见的图表类型。散点图由许多“点”组成，有时，这些点用来表示数据在坐标系中的位置（比如在笛卡尔坐标系下，表示数据在 x 轴和 y 轴上的坐标；在地图坐标系下，表示数据在地图上的某个位置等）；有时，这些点的大小、颜色等属性也可以映射到数据值，用以表现高维数据。</p><h3 id="最简单的散点图" tabindex="-1"><a class="header-anchor" href="#最简单的散点图" aria-hidden="true">#</a> 最简单的散点图</h3><p>下面是一个横坐标为类目轴、纵坐标为数值轴的最简单的散点图配置：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;Sun&#39;, &#39;Mon&#39;, &#39;Tue&#39;, &#39;Wed&#39;, &#39;Thu&#39;, &#39;Fri&#39;, &#39;Sat&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;scatter&#39;,
      data: [220, 182, 191, 234, 290, 330, 310]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="笛卡尔坐标系下的散点图" tabindex="-1"><a class="header-anchor" href="#笛卡尔坐标系下的散点图" aria-hidden="true">#</a> 笛卡尔坐标系下的散点图</h3><p>在上文的例子中，散点图的横坐标都是离散的类目轴，而纵坐标都是连续的数值轴。而对于散点图而言，另一种常见的场景是，两个坐标轴均为连续的数值轴，也就是笛卡尔坐标系。这时的系列形式略有不同，数据的横坐标和纵坐标一同写在 data 中，而非坐标轴中。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {},
  yAxis: {},
  series: [
    {
      type: &#39;scatter&#39;,
      data: [
        [10, 5],
        [0, 8],
        [6, 10],
        [2, 12],
        [8, 9]
      ]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="散点图样式设置" tabindex="-1"><a class="header-anchor" href="#散点图样式设置" aria-hidden="true">#</a> 散点图样式设置</h3><h4 id="图形的形状" tabindex="-1"><a class="header-anchor" href="#图形的形状" aria-hidden="true">#</a> 图形的形状</h4><p>图形（symbol）指的是散点图中数据“点”的形状。有三类图形可选，一种是 ECharts 内置形状，第二种是图片，第三种是 SVG 的路径。</p><p>ECharts 内置形状包括：圆形、矩形、圆角矩形、三角形、菱形、大头针形、箭头形，分别对应<code>&#39;circle&#39;</code>、<code>&#39;rect&#39;</code>、<code>&#39;roundRect&#39;</code>、<code>&#39;triangle&#39;</code>、<code>&#39;diamond&#39;</code>、<code>&#39;pin&#39;</code>、<code>&#39;arrow&#39;</code>。使用内置形状时，只要将 symbol 属性指定为形状名称对应的字符串即可。</p><p>如果想要将图形指定为任意的图片，以 <code>&#39;image://&#39;</code> 开头，后面跟图片的绝对或相对地址。形如：<code>&#39;image://http://example.com/xxx.png&#39;</code> 或 <code>&#39;image://./xxx.png&#39;</code>。</p><p>除此之外，还支持 SVG 的路径作为矢量图形，将 <code>symbol</code> 设置为以 <code>&#39;path://&#39;</code> 开头的 SVG 路径即可。使用矢量图形的好处是，图片不会因为缩放而产生锯齿或模糊，并且通常而言比图片形式的文件大小更小。路径的查看方法为，打开一个 <code>SVG</code> 文件，找到形如 <code>&lt;path d=&quot;M… L…&quot;&gt;&lt;/path&gt;</code> 的路径，将 <code>d</code> 的值添加在 <code>&#39;path://&#39;</code> 后即可。</p><p>下面，我们展示一个将图形设置为矢量爱心形状的方式。</p><p>首先，我们需要一个爱心的 SVG 文件，可以使用矢量编辑软件绘制，或者从网上下载到相关资源。其内容如下：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;?xml version=&quot;1.0&quot; encoding=&quot;iso-8859-1&quot;?&gt;
&lt;svg version=&quot;1.1&quot; xmlns=&quot;http://www.w3.org/2000/svg&quot; xmlns:xlink=&quot;http://www.w3.org/1999/xlink&quot; x=&quot;0px&quot; y=&quot;0px&quot; viewBox=&quot;0 0 51.997 51.997&quot; style=&quot;enable-background:new 0 0 51.997 51.997;&quot; xml:space=&quot;preserve&quot;&gt;
    &lt;path d=&quot;M51.911,16.242C51.152,7.888,45.239,1.827,37.839,1.827c-4.93,0-9.444,2.653-11.984,6.905 c-2.517-4.307-6.846-6.906-11.697-6.906c-7.399,0-13.313,6.061-14.071,14.415c-0.06,0.369-0.306,2.311,0.442,5.478 c1.078,4.568,3.568,8.723,7.199,12.013l18.115,16.439l18.426-16.438c3.631-3.291,6.121-7.445,7.199-12.014 C52.216,18.553,51.97,16.611,51.911,16.242z&quot;/&gt;
&lt;/svg&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在 ECharts 的 <code>symbol</code> 配置项中，我们使用 <code>d</code> 的值作为路径。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;Sun&#39;, &#39;Mon&#39;, &#39;Tue&#39;, &#39;Wed&#39;, &#39;Thu&#39;, &#39;Fri&#39;, &#39;Sat&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;scatter&#39;,
      data: [220, 182, 191, 234, 290, 330, 310],
      symbolSize: 20,
      symbol:
        &#39;path://M51.911,16.242C51.152,7.888,45.239,1.827,37.839,1.827c-4.93,0-9.444,2.653-11.984,6.905 c-2.517-4.307-6.846-6.906-11.697-6.906c-7.399,0-13.313,6.061-14.071,14.415c-0.06,0.369-0.306,2.311,0.442,5.478 c1.078,4.568,3.568,8.723,7.199,12.013l18.115,16.439l18.426-16.438c3.631-3.291,6.121-7.445,7.199-12.014 C52.216,18.553,51.97,16.611,51.911,16.242z&#39;
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>这样，就能得到爱心形状的图形作为点的形状了。</p><h4 id="图形的大小" tabindex="-1"><a class="header-anchor" href="#图形的大小" aria-hidden="true">#</a> 图形的大小</h4><p>图形大小可以使用 <code>series.symbolSize</code> 控制。它既可以是一个表示图形大小的像素值，也可以是一个包含两个 number 元素的数组，分别表示图形的宽和高。</p><p>除此之外，它还可以是一个回调函数，其参数格式为：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>(value: Array | number, params: Object) =&gt; number | Array;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>第一个参数为数据值，第二个参数是数据项的其他参数。</p><p>在下面的例子中，我们将散点图点的大小设置为与其数据值成正比。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  xAxis: {
    data: [&#39;Sun&#39;, &#39;Mon&#39;, &#39;Tue&#39;, &#39;Wed&#39;, &#39;Thu&#39;, &#39;Fri&#39;, &#39;Sat&#39;]
  },
  yAxis: {},
  series: [
    {
      type: &#39;scatter&#39;,
      data: [220, 182, 191, 234, 290, 330, 310],
      symbolSize: function(value) {
        return value / 10;
      }
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,27),l=[a];function c(r,t){return i(),n("div",null,l)}const u=e(s,[["render",c],["__file","09-41.html.vue"]]);export{u as default};
