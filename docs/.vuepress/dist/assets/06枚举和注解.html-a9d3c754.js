import{_ as e,p as i,q as n,a1 as l}from"./framework-7db056f4.js";const s={},a=l(`<p>枚举有个隐式继承extends java.lang.Eunm，不能再继承了。</p><p>枚举：把具体的对象一个一个列举出来的类</p><p>枚举是一组<strong>常量</strong>的集合，它属于一种特殊的类，里面只包含一组有限的特定的对象</p><p>枚举类型的构造方法，只能用private私有</p><p>可以有抽象方法，但要使用匿名内部类</p><p>枚举对象值通常为只读</p><ol><li><p>将构造器私有化，防止直接new</p></li><li><p>去掉setXxx方法，防止属性被修改</p></li><li><p>在类内部，直接创建固定对象，用final + static修饰</p></li><li><p>通过enum来实现枚举类：</p></li></ol><ul><li><p>enum ---&gt; 代替class</p></li><li><p><strong>常量名1（实参列表），常量名2（实参列表）；</strong></p></li><li><p>枚举对象必须放在枚举类的行首</p></li></ul><div class="language-Java line-numbers-mode" data-ext="Java"><pre class="language-Java"><code>public enum Week {
    MON,
    TUE,
    WED,
    THU,
    FRI,
    SAT,
    SUN;
}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>values(): 获取枚举中定义的所有常量对象</p><div class="language-Java line-numbers-mode" data-ext="Java"><pre class="language-Java"><code>HttpStatus[] values = HttpStatus.values();
for (HttpStatus value : values) {
	System.out.println(value);
}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>枚举方法</p><p>values 获取所有的枚举对象，返回一个数组</p><p>枚举项之间用==比较</p><div class="language-Java line-numbers-mode" data-ext="Java"><pre class="language-Java"><code>package com.xxx.test2;

public enum Color2{
    BLUE{
        @Override
        public String getColor() {
            return &quot;蓝色&quot;;
        }
    },RED{
        @Override
        public String getColor() {
            return null;
        }
    },PINK {
        @Override
        public String getColor() {
            return null;
        }
    };

    private String name;
    //可以有抽象方法，但要使用匿名内部类
    public abstract String getColor();
}

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="注解" tabindex="-1"><a class="header-anchor" href="#注解" aria-hidden="true">#</a> 注解</h3><p>注解是修饰Java代码中的类、方法、属性、参数的一种特殊“注释”； 但是它又和注释不尽相同,注释(除文档型注释)会被编译器忽略,而注解会被编译到.class文件中。</p><p>@Override 检查该方法是否是重写方法</p><p>@Deprecated 标记过时方法</p><p>@SuppressWarnings 指示编译器去忽略注解中声明的警告</p>`,20),d=[a];function v(r,u){return i(),n("div",null,d)}const t=e(s,[["render",v],["__file","06枚举和注解.html.vue"]]);export{t as default};
