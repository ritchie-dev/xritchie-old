import{_ as e,p as i,q as n,a1 as d}from"./framework-7db056f4.js";const s={},l=d(`<h2 id="基础饼图" tabindex="-1"><a class="header-anchor" href="#基础饼图" aria-hidden="true">#</a> 基础饼图</h2><p>饼图主要用于表现不同类目的数据在总和中的占比。每个的弧度表示数据数量的比例。</p><h3 id="最简单的饼图" tabindex="-1"><a class="header-anchor" href="#最简单的饼图" aria-hidden="true">#</a> 最简单的饼图</h3><p>饼图的配置和折线图、柱状图略有不同，不再需要配置坐标轴，而是把数据名称和值都写在系列中。以下是一个最简单的饼图的例子。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  series: [
    {
      type: &#39;pie&#39;,
      data: [
        {
          value: 335,
          name: &#39;直接访问&#39;
        },
        {
          value: 234,
          name: &#39;联盟广告&#39;
        },
        {
          value: 1548,
          name: &#39;搜索引擎&#39;
        }
      ]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>需要注意的是，这里是 <code>value</code> 不需要是百分比数据，ECharts 会根据所有数据的 <code>value</code> ，按比例分配它们在饼图中对应的弧度。</p><h3 id="饼图样式设置" tabindex="-1"><a class="header-anchor" href="#饼图样式设置" aria-hidden="true">#</a> 饼图样式设置</h3><h4 id="饼图的半径" tabindex="-1"><a class="header-anchor" href="#饼图的半径" aria-hidden="true">#</a> 饼图的半径</h4><p>饼图的半径可以通过 <code>series.radius</code> 设置，可以是诸如<code> &#39;60%&#39;</code> 这样相对的百分比字符串，或是 <code>200</code> 这样的绝对像素数值。当它是百分比字符串时，它是相对于容器宽高中较小的一条边的。也就是说，如果宽度大于高度，则百分比是相对于高度的，反之则反；当它是数值型时，它表示绝对的像素大小。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  series: [
    {
      type: &#39;pie&#39;,
      data: [
        {
          value: 335,
          name: &#39;直接访问&#39;
        },
        {
          value: 234,
          name: &#39;联盟广告&#39;
        },
        {
          value: 1548,
          name: &#39;搜索引擎&#39;
        }
      ],
      radius: &#39;50%&#39;
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="如果数据和为-0-不显示饼图" tabindex="-1"><a class="header-anchor" href="#如果数据和为-0-不显示饼图" aria-hidden="true">#</a> 如果数据和为 0，不显示饼图</h3><p>在默认情况下，如果数据值和为 0，会显示平均分割的扇形。比如，如果有 4 个数据项，并且每个数据项都是 0，则每个扇形都是 90°。如果我们希望在这种情况下不显示任何扇形，可以将 <code>series.stillShowZeroSum</code> 设为 <code>false</code>。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  series: [
    {
      type: &#39;pie&#39;,
      stillShowZeroSum: false,
      data: [
        {
          value: 0,
          name: &#39;直接访问&#39;
        },
        {
          value: 0,
          name: &#39;联盟广告&#39;
        },
        {
          value: 0,
          name: &#39;搜索引擎&#39;
        }
      ]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>如果希望扇形对应的标签也不显示，可以将 <code>series.label.show</code> 设为 <code>false</code>。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>option = {
  series: [
    {
      type: &#39;pie&#39;,
      stillShowZeroSum: false,
      label: {
        show: false
      },
      data: [
        {
          value: 0,
          name: &#39;直接访问&#39;
        },
        {
          value: 0,
          name: &#39;联盟广告&#39;
        },
        {
          value: 0,
          name: &#39;搜索引擎&#39;
        }
      ]
    }
  ]
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,15),a=[l];function v(r,c){return i(),n("div",null,a)}const m=e(s,[["render",v],["__file","09-31.html.vue"]]);export{m as default};
