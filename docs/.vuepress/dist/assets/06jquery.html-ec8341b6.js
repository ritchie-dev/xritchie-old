import{_ as e,p as i,q as t,a1 as l}from"./framework-7db056f4.js";const n={},d=l(`<h3 id="jquery是什么" tabindex="-1"><a class="header-anchor" href="#jquery是什么" aria-hidden="true">#</a> JQuery是什么</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>JQuery是一个快速、简洁的JavaScript代码库(轻量级JS框架)
JQuery设计的宗旨是&quot;Write Less,Do More&quot;,即倡导写更少的代码,做更多的事情.
JQuery封装JavaScript常用的功能代码,提供一种简便的JavaScript操作方式,优化HTML文档操作、事件处理、动画设计和Ajax交互
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery的优势" tabindex="-1"><a class="header-anchor" href="#jquery的优势" aria-hidden="true">#</a> JQuery的优势</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code> - 轻量级
 - 强大的选择器
 - 出色的 DOM 操作的封装
 - 可靠的事件处理机制
 - 完善的 Ajax
 - 出色的浏览器兼容性
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery的引入方式" tabindex="-1"><a class="header-anchor" href="#jquery的引入方式" aria-hidden="true">#</a> JQuery的引入方式</h3><ul><li>离线本地引入</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>下载地址: https://www.jq22.com/jquery-info122

1. 在head中引入
2. 在body中引入

&lt;script src=&quot;jquery-1.12.2.min.js&quot;&gt;&lt;/script&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>CDN引入</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>CDN地址: https://www.jq22.com/jquery-info122

1. 在head中引入
2. 在body中引入

&lt;script src=&quot;https://code.jquery.com/jquery-3.5.1.min.js&quot;&gt;&lt;/script&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery入门之helloworld" tabindex="-1"><a class="header-anchor" href="#jquery入门之helloworld" aria-hidden="true">#</a> JQuery入门之HelloWorld</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;button id=&quot;btn&quot;&gt;Hello JQuery&lt;/button&gt;

$(function(){
	$(&quot;#btn&quot;).click(function(){
		alert(&quot;HelloWorld.&quot;);
	});
});
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery文档使用之选择器" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之选择器" aria-hidden="true">#</a> JQuery文档使用之选择器</h3><h4 id="基本选择器一" tabindex="-1"><a class="header-anchor" href="#基本选择器一" aria-hidden="true">#</a> 基本选择器一</h4><ul><li>id选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div id=&quot;notMe&quot;&gt;&lt;p&gt;id=&quot;notMe&quot;&lt;/p&gt;&lt;/div&gt;
&lt;div id=&quot;myDiv&quot;&gt;id=&quot;myDiv&quot;&lt;/div&gt;

JS:
$(&quot;#myDiv&quot;);

结果:
[ &lt;div id=&quot;myDiv&quot;&gt;id=&quot;myDiv&quot;&lt;/div&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>元素(标签)选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div&gt;DIV1&lt;/div&gt;
&lt;div&gt;DIV2&lt;/div&gt;
&lt;span&gt;SPAN&lt;/span&gt;

JS:
$(&quot;div&quot;);

结果:
[ &lt;div&gt;DIV1&lt;/div&gt;, &lt;div&gt;DIV2&lt;/div&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>类选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div class=&quot;notMe&quot;&gt;div class=&quot;notMe&quot;&lt;/div&gt;
&lt;div class=&quot;myClass&quot;&gt;div class=&quot;myClass&quot;&lt;/div&gt;
&lt;span class=&quot;myClass&quot;&gt;span class=&quot;myClass&quot;&lt;/span&gt;

JS:
$(&quot;.myClass&quot;);

结果:
[ &lt;div class=&quot;myClass&quot;&gt;div class=&quot;myClass&quot;&lt;/div&gt;, &lt;span class=&quot;myClass&quot;&gt;span class=&quot;myClass&quot;&lt;/span&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>通用选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div&gt;DIV&lt;/div&gt;
&lt;span&gt;SPAN&lt;/span&gt;
&lt;p&gt;P&lt;/p&gt;

JS:
$(&quot;*&quot;);

结果:
[ &lt;div&gt;DIV&lt;/div&gt;, &lt;span&gt;SPAN&lt;/span&gt;, &lt;p&gt;P&lt;/p&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>分组选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div&gt;div&lt;/div&gt;
&lt;p class=&quot;myClass&quot;&gt;p class=&quot;myClass&quot;&lt;/p&gt;
&lt;span&gt;span&lt;/span&gt;
&lt;p class=&quot;notMyClass&quot;&gt;p class=&quot;notMyClass&quot;&lt;/p&gt;

JS:
$(&quot;div,span,p.myClass&quot;)

结果:
[ &lt;div&gt;div&lt;/div&gt;, &lt;p class=&quot;myClass&quot;&gt;p class=&quot;myClass&quot;&lt;/p&gt;, &lt;span&gt;span&lt;/span&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="基本选择器二" tabindex="-1"><a class="header-anchor" href="#基本选择器二" aria-hidden="true">#</a> 基本选择器二</h4><ul><li>匹配索引值为偶数的元素</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;table&gt;
  &lt;tr&gt;&lt;td&gt;Header 1&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;Value 1&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;Value 2&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

JS:
$(&quot;tr:even&quot;)

结果:
[ &lt;tr&gt;&lt;td&gt;Header 1&lt;/td&gt;&lt;/tr&gt;, &lt;tr&gt;&lt;td&gt;Value 2&lt;/td&gt;&lt;/tr&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>匹配索引值为奇数的元素</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;table&gt;
  &lt;tr&gt;&lt;td&gt;Header 1&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;Value 1&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;Value 2&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

JS:
$(&quot;tr:odd&quot;)

结果:
[ &lt;tr&gt;&lt;td&gt;Value 1&lt;/td&gt;&lt;/tr&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="层级选择器" tabindex="-1"><a class="header-anchor" href="#层级选择器" aria-hidden="true">#</a> 层级选择器</h4><ul><li>后代选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;label&gt;Name:&lt;/label&gt;
  &lt;input name=&quot;name&quot; /&gt;
  &lt;fieldset&gt;
      &lt;label&gt;Newsletter:&lt;/label&gt;
      &lt;input name=&quot;newsletter&quot; /&gt;
 &lt;/fieldset&gt;
&lt;/form&gt;
&lt;input name=&quot;none&quot; /&gt;

JS:
$(&quot;form input&quot;)

结果:
[ &lt;input name=&quot;name&quot; /&gt;, &lt;input name=&quot;newsletter&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>子类选择器</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;label&gt;Name:&lt;/label&gt;
  &lt;input name=&quot;name&quot; /&gt;
  &lt;fieldset&gt;
      &lt;label&gt;Newsletter:&lt;/label&gt;
      &lt;input name=&quot;newsletter&quot; /&gt;
 &lt;/fieldset&gt;
&lt;/form&gt;
&lt;input name=&quot;none&quot; /&gt;

JS:
$(&quot;form &gt; input&quot;)

结果:
[ &lt;input name=&quot;name&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><p>兄弟选择器</p></li><li><ul><li>匹配所有紧接在 prev 元素后的 next 元素</li></ul></li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;label&gt;Name:&lt;/label&gt;
  &lt;input name=&quot;name&quot; /&gt;
  &lt;fieldset&gt;
      &lt;label&gt;Newsletter:&lt;/label&gt;
      &lt;input name=&quot;newsletter&quot; /&gt;
 &lt;/fieldset&gt;
&lt;/form&gt;
&lt;input name=&quot;none&quot; /&gt;

JS:
$(&quot;label + input&quot;)

结果:
[ &lt;input name=&quot;name&quot; /&gt;, &lt;input name=&quot;newsletter&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><ul><li>匹配 prev 元素的所有同辈 siblings 元素</li></ul></li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;label&gt;Name:&lt;/label&gt;
  &lt;input name=&quot;name&quot; /&gt;
  &lt;fieldset&gt;
      &lt;label&gt;Newsletter:&lt;/label&gt;
      &lt;input name=&quot;newsletter&quot; /&gt;
 &lt;/fieldset&gt;
&lt;/form&gt;
&lt;input name=&quot;none&quot; /&gt;

JS:
$(&quot;form ~ input&quot;)

结果:
[ &lt;input name=&quot;none&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="属性选择器" tabindex="-1"><a class="header-anchor" href="#属性选择器" aria-hidden="true">#</a> 属性选择器</h4><ul><li>根据属性名匹配</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div&gt;
  &lt;p&gt;Hello!&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&quot;test2&quot;&gt;&lt;/div&gt;

JS:
$(&quot;div[id]&quot;)

结果:
[ &lt;div id=&quot;test2&quot;&gt;&lt;/div&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>根据属性名和值匹配</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot;  /&gt;
&lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; /&gt;
&lt;input type=&quot;checkbox&quot; name=&quot;accept&quot; /&gt;

JS:
$(&quot;input[name=&#39;newsletter&#39;]&quot;)

结果:
[ &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot;  /&gt;, &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="表单选择器" tabindex="-1"><a class="header-anchor" href="#表单选择器" aria-hidden="true">#</a> 表单选择器</h4><ul><li>匹配单行文本框</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;input type=&quot;text&quot; /&gt;
  &lt;input type=&quot;checkbox&quot; /&gt;
  &lt;input type=&quot;radio&quot; /&gt;
  &lt;input type=&quot;image&quot; /&gt;
  &lt;input type=&quot;file&quot; /&gt;
  &lt;input type=&quot;submit&quot; /&gt;
  &lt;input type=&quot;reset&quot; /&gt;
  &lt;input type=&quot;password&quot; /&gt;
  &lt;input type=&quot;button&quot; /&gt;
  &lt;select&gt;&lt;option/&gt;&lt;/select&gt;
  &lt;textarea&gt;&lt;/textarea&gt;
  &lt;button&gt;&lt;/button&gt;
&lt;/form&gt;

JS:
$(&quot;:text&quot;)

结果:
[ &lt;input type=&quot;text&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>匹配密码框</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;input type=&quot;text&quot; /&gt;
  &lt;input type=&quot;checkbox&quot; /&gt;
  &lt;input type=&quot;radio&quot; /&gt;
  &lt;input type=&quot;image&quot; /&gt;
  &lt;input type=&quot;file&quot; /&gt;
  &lt;input type=&quot;submit&quot; /&gt;
  &lt;input type=&quot;reset&quot; /&gt;
  &lt;input type=&quot;password&quot; /&gt;
  &lt;input type=&quot;button&quot; /&gt;
  &lt;select&gt;&lt;option/&gt;&lt;/select&gt;
  &lt;textarea&gt;&lt;/textarea&gt;
  &lt;button&gt;&lt;/button&gt;
&lt;/form&gt;

JS:
$(&quot;:password&quot;)

结果:
[ &lt;input type=&quot;password&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>匹配单选框</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;input type=&quot;text&quot; /&gt;
  &lt;input type=&quot;checkbox&quot; /&gt;
  &lt;input type=&quot;radio&quot; /&gt;
  &lt;input type=&quot;image&quot; /&gt;
  &lt;input type=&quot;file&quot; /&gt;
  &lt;input type=&quot;submit&quot; /&gt;
  &lt;input type=&quot;reset&quot; /&gt;
  &lt;input type=&quot;password&quot; /&gt;
  &lt;input type=&quot;button&quot; /&gt;
  &lt;select&gt;&lt;option/&gt;&lt;/select&gt;
  &lt;textarea&gt;&lt;/textarea&gt;
  &lt;button&gt;&lt;/button&gt;
&lt;/form&gt;

JS:
$(&quot;:radio&quot;)

结果:
[ &lt;input type=&quot;radio&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>匹配复选框</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;input type=&quot;text&quot; /&gt;
  &lt;input type=&quot;checkbox&quot; /&gt;
  &lt;input type=&quot;radio&quot; /&gt;
  &lt;input type=&quot;image&quot; /&gt;
  &lt;input type=&quot;file&quot; /&gt;
  &lt;input type=&quot;submit&quot; /&gt;
  &lt;input type=&quot;reset&quot; /&gt;
  &lt;input type=&quot;password&quot; /&gt;
  &lt;input type=&quot;button&quot; /&gt;
  &lt;select&gt;&lt;option/&gt;&lt;/select&gt;
  &lt;textarea&gt;&lt;/textarea&gt;
  &lt;button&gt;&lt;/button&gt;
&lt;/form&gt;

JS:
$(&quot;:checkbox&quot;)

结果:
[ &lt;input type=&quot;checkbox&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>匹配不可见元素</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;table&gt;
  &lt;tr style=&quot;display:none&quot;&gt;&lt;td&gt;Value 1&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;Value 2&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

JS:
$(&quot;tr:hidden&quot;)

结果:
[ &lt;tr style=&quot;display:none&quot;&gt;&lt;td&gt;Value 1&lt;/td&gt;&lt;/tr&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="表单对象属性选择器" tabindex="-1"><a class="header-anchor" href="#表单对象属性选择器" aria-hidden="true">#</a> 表单对象属性选择器</h4><ul><li>匹配所有选中的元素</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;form&gt;
  &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; checked=&quot;checked&quot; value=&quot;Daily&quot; /&gt;
  &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; value=&quot;Weekly&quot; /&gt;
  &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; checked=&quot;checked&quot; value=&quot;Monthly&quot; /&gt;
&lt;/form&gt;

JS:
$(&quot;input:checked&quot;)

结果:
[ &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; checked=&quot;checked&quot; value=&quot;Daily&quot; /&gt;, &lt;input type=&quot;checkbox&quot; name=&quot;newsletter&quot; checked=&quot;checked&quot; value=&quot;Monthly&quot; /&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>匹配所有选中的option元素</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;select&gt;
  &lt;option value=&quot;1&quot;&gt;Flowers&lt;/option&gt;
  &lt;option value=&quot;2&quot; selected=&quot;selected&quot;&gt;Gardens&lt;/option&gt;
  &lt;option value=&quot;3&quot;&gt;Trees&lt;/option&gt;
&lt;/select&gt;

JS:
$(&quot;select option:selected&quot;)

结果:
[ &lt;option value=&quot;2&quot; selected=&quot;selected&quot;&gt;Gardens&lt;/option&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery文档使用之文档处理" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之文档处理" aria-hidden="true">#</a> JQuery文档使用之文档处理</h3><ul><li>内部插入</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;p&gt;I would like to say: &lt;/p&gt;

JS:
$(&quot;p&quot;).append(&quot;&lt;b&gt;Hello&lt;/b&gt;&quot;);

结果:
[ &lt;p&gt;I would like to say: &lt;b&gt;Hello&lt;/b&gt;&lt;/p&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>删除</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;p&gt;Hello, &lt;span&gt;Person&lt;/span&gt; &lt;a href=&quot;#&quot;&gt;and person&lt;/a&gt;&lt;/p&gt;

JS:
$(&quot;p&quot;).empty();

结果:
&lt;p&gt;&lt;/p&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery文档使用之筛选" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之筛选" aria-hidden="true">#</a> JQuery文档使用之筛选</h3><ul><li>查询列表中的第一个</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;ul&gt;
    &lt;li&gt;list item 1&lt;/li&gt;
    &lt;li&gt;list item 2&lt;/li&gt;
    &lt;li&gt;list item 3&lt;/li&gt;
    &lt;li&gt;list item 4&lt;/li&gt;
    &lt;li&gt;list item 5&lt;/li&gt;
&lt;/ul&gt;

JS:
$(&#39;li&#39;).first()

结果:
[ &lt;li&gt;list item 1&lt;/li&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>查询列表中的最后一个</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;ul&gt;
    &lt;li&gt;list item 1&lt;/li&gt;
    &lt;li&gt;list item 2&lt;/li&gt;
    &lt;li&gt;list item 3&lt;/li&gt;
    &lt;li&gt;list item 4&lt;/li&gt;
    &lt;li&gt;list item 5&lt;/li&gt;
&lt;/ul&gt;

JS:
$(&#39;li&#39;).last()

结果:
[ &lt;li&gt;list item 5&lt;/li&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>筛选出与指定表达式匹配的元素集合</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;p&gt;Hello&lt;/p&gt;&lt;p&gt;Hello Again&lt;/p&gt;&lt;p class=&quot;selected&quot;&gt;And Again&lt;/p&gt;

JS:
$(&quot;p&quot;).filter(&quot;.selected&quot;)

结果:
[ &lt;p class=&quot;selected&quot;&gt;And Again&lt;/p&gt; ]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>筛选一个包含着所有匹配元素的唯一父元素的元素集合</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>HTML:
&lt;div&gt;&lt;p&gt;Hello&lt;/p&gt;&lt;p&gt;Hello&lt;/p&gt;&lt;/div&gt;

JS:
$(&quot;p&quot;).parent()

结果:
[ &lt;div&gt;&lt;p&gt;Hello&lt;/p&gt;&lt;p&gt;Hello&lt;/p&gt;&lt;/div&gt;]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery文档使用之事件绑定" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之事件绑定" aria-hidden="true">#</a> JQuery文档使用之事件绑定</h3><ul><li>页面载入事件</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>当DOM载入就绪可以查询及操纵时绑定一个要执行的函数

$(document).ready(function(){
  // 在这里写你的代码...
});

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><p>事件绑定</p></li><li><ul><li>on函数</li></ul></li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;p&quot;).on(&quot;click&quot;, function(){
	alert($(this).text());
});
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><ul><li>bind函数</li></ul></li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;p&quot;).bind(&quot;click&quot;, function(){
  alert($(this).text());
});
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><ul><li>直接绑定</li></ul></li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;p&quot;).click(function(){
	alert($(this).text());
});
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>单击事件</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;p&quot;).click();
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ul><li>失去焦点事件</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;input&quot;).blur();
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ul><li>获取焦点事件</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;input&quot;).focus();
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h3 id="jquery文档使用之属性操作" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之属性操作" aria-hidden="true">#</a> JQuery文档使用之属性操作</h3><ul><li>attr()获取/设置元素属性</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 获取属性   $(&quot;img&quot;).attr(&quot;src&quot;);
2. 设置属性   $(&quot;img&quot;).attr({ src: &quot;test.jpg&quot;, alt: &quot;Test Image&quot; });
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>addClass()给元素添加类</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$(&quot;p&quot;).addClass(&quot;selected&quot;);
$(&quot;p&quot;).addClass(&quot;selected1 selected2&quot;);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>html()获取/设置第一个匹配元素的html内容</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 获取值  $(&#39;p&#39;).html();
2. 设置值  $(&quot;p&quot;).html(&quot;Hello &lt;b&gt;world&lt;/b&gt;!&quot;);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>text()获取/设置第一个匹配元素的文本内容</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 获取值  $(&#39;p&#39;).text();
2. 设置值  $(&quot;p&quot;).text(&quot;Hello world!&quot;);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>val()获取/设置input元素value属性值</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 获取值  $(&quot;input&quot;).val();
2. 设置值  $(&quot;input&quot;).val(&quot;hello world!&quot;);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery文档使用之css操作" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之css操作" aria-hidden="true">#</a> JQuery文档使用之CSS操作</h3><ul><li>css()获取/设置css样式</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 获取值  $(&quot;p&quot;).css(&quot;color&quot;);
2. 设置值  $(&quot;p&quot;).css({ &quot;color&quot;: &quot;#ff0011&quot;, &quot;background&quot;: &quot;blue&quot; });
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery文档使用之效果" tabindex="-1"><a class="header-anchor" href="#jquery文档使用之效果" aria-hidden="true">#</a> JQuery文档使用之效果</h3><ul><li>元素隐藏和显示</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 显示  

HTML: &lt;p style=&quot;display: none&quot;&gt;Hello&lt;/p&gt;
JS  : $(&quot;p&quot;).show()

2. 隐藏

JS  : $(&quot;p&quot;).hide()
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>滑动</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1. 向上收起(隐藏) slideUp()
2. 向下展开(显示) slideDown()
3. 隐藏和显示切换 slideToggle()
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery节点创建" tabindex="-1"><a class="header-anchor" href="#jquery节点创建" aria-hidden="true">#</a> JQuery节点创建</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;table border=&quot;1&quot;&gt;&lt;/table&gt;

//创建&lt;tr&gt;&lt;/tr&gt;
var trEle = $(&quot;&lt;tr&gt;&lt;/tr&gt;&quot;);
var thEle1 = $(&quot;&lt;th&gt;ID&lt;/th&gt;&quot;);
var thEle2 = $(&quot;&lt;th&gt;名字&lt;/th&gt;&quot;);
var thEle3 = $(&quot;&lt;th&gt;性别&lt;/th&gt;&quot;);
trEle.append(thEle1);
trEle.append(thEle2);
trEle.append(thEle3);
var trEle2 = $(&quot;&lt;tr&gt;&lt;/tr&gt;&quot;);
var tdEle1 = $(&quot;&lt;td&gt;1001&lt;/td&gt;&quot;);
var tdEle2 = $(&quot;&lt;td&gt;张三&lt;/td&gt;&quot;);
var tdEle3 = $(&quot;&lt;td&gt;男&lt;/td&gt;&quot;);
trEle2.append(tdEle1);
trEle2.append(tdEle2);
trEle2.append(tdEle3);
console.log(trEle[0]);
$(&quot;table&quot;).append(trEle);
$(&quot;table&quot;).append(trEle2);
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery对象和dom对象的转换" tabindex="-1"><a class="header-anchor" href="#jquery对象和dom对象的转换" aria-hidden="true">#</a> JQuery对象和DOM对象的转换</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;button id=&quot;btn&quot;&gt;我是一个按钮&lt;/button&gt;

//使用jQuery选择器获取jQuery对象
var $btn = $(&quot;#btn&quot;);
console.log($btn);
//将jQuery对象转换成DOM对象
console.log($btn[0]);//方式一
console.log($btn.get(0)); //方式二
//将DOM对象转成jQuery对象
console.log($($btn[0]));
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="jquery中-get、-post、-getjson、-ajax-方法区别" tabindex="-1"><a class="header-anchor" href="#jquery中-get、-post、-getjson、-ajax-方法区别" aria-hidden="true">#</a> jQuery中$.get、$.post、$.getJSON、$.ajax 方法区别？</h3><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>jQuery.get() ：使用 HTTP GET 请求从服务器加载数据；
jQuery.post()：使用 HTTP POST 请求从服务器加载数据；

jQuery.getJSON()：使用 HTTP GET 请求从服务器加载 JSON 编码数据；
jQuery.ajax()：执行异步 HTTP (Ajax) 请求；
$ajax方法，需解析Json字符串；
$.getJSON方法需解析JSON对象；
两种方法的效果是一样的，区别在于：$ajax 的data参数格式data: &quot;q=&quot; + str，与$.getJSON中的参数格式{q:str}的区别，并且在返回的结果中，$ajax返回的是Json字符串，需用eval方法转化为JSON对象，而$.getJSON返回的是JSON对象，可以直接使用，而$.getJSON的写法也更加简单，推荐使用。
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,112),s=[d];function a(u,v){return i(),t("div",null,s)}const c=e(n,[["render",a],["__file","06jquery.html.vue"]]);export{c as default};
