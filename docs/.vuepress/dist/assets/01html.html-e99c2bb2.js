import{_ as n,p as s,q as a,a1 as e}from"./framework-7db056f4.js";const t={},l=e(`<h2 id="web" tabindex="-1"><a class="header-anchor" href="#web" aria-hidden="true">#</a> web</h2><blockquote><p>在服务器端编程语言中，java更适合开发大型web应用，因为更适合处理复杂的业务逻辑，有成熟的多线程模型，提供了多种开源类库，避免了重复造轮子，降低了开发成本。</p></blockquote><blockquote><p>web:www(word wide web) Internet:全球性的互联网络，基于这个网络提供了很多重要的服务，比如telnet实现远程登陆，email实现邮件收发，而www是基于Internet实现的一个多媒体信息服务，因此web是Internet提供的众多服务中的一种。</p></blockquote><blockquote><p>web服务与其他服务相比的特点: web服务是基于B/S结构的一种服务，B即Browser，S即Server，使用浏览器通过通讯协议发送请求，以获取服务器发送回来的响应，从而达到浏览器网页的目的。<br> web三要素:浏览器，服务器，http通讯协议<br> web浏览器:<br> 早期被翻译成客户，因为浏览器就是帮助(代理)我们访问者把请求交给服务器，并且将服务器返回的资源以图形化的方式在界面上显示出来。比如:服务器返回了html源代码，浏览器需要作为html解释器和内嵌脚本执行器将源码翻译成可视化的页面。<br> 网页编程基础:html+css+js<br> html用来构建网页结构，css控制页面的外观，js为网页添加动态交互效果</p></blockquote><h2 id="html" tabindex="-1"><a class="header-anchor" href="#html" aria-hidden="true">#</a> HTML</h2><h4 id="概述" tabindex="-1"><a class="header-anchor" href="#概述" aria-hidden="true">#</a> 概述</h4><blockquote><p>hypertext markup language是一种超文本标记语言，用来构建网页文件，以.html/.htm为后缀，由浏览器解释运行，可以使用css定义样式，嵌入js代码实现动态效果。</p></blockquote><h4 id="html文件的基本格式" tabindex="-1"><a class="header-anchor" href="#html文件的基本格式" aria-hidden="true">#</a> HTML文件的基本格式</h4><div class="language-html line-numbers-mode" data-ext="html"><pre class="language-html"><code><span class="token doctype"><span class="token punctuation">&lt;!</span><span class="token doctype-tag">DOCTYPE</span> <span class="token name">html</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>html</span><span class="token punctuation">&gt;</span></span>
	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>head</span><span class="token punctuation">&gt;</span></span>
		
	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>head</span><span class="token punctuation">&gt;</span></span>
	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>body</span><span class="token punctuation">&gt;</span></span>
	
	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>body</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>html</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="语法标记" tabindex="-1"><a class="header-anchor" href="#语法标记" aria-hidden="true">#</a> 语法标记</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>封闭类型:双标记，比如&lt;title&gt;***&lt;/title&gt;
非封闭类型:单标记或空标记，比如&lt;meta/&gt; &lt;br/&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="html基本结构标签" tabindex="-1"><a class="header-anchor" href="#html基本结构标签" aria-hidden="true">#</a> html基本结构标签</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;html&gt;&lt;/html&gt;:根标签
&lt;head&gt;&lt;/head&gt;:头标签，为页面添加全局信息，比如定义页面标题，页面刷新速度...
&lt;body&gt;&lt;/body&gt;:页面主题标签
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="head中常见的标签" tabindex="-1"><a class="header-anchor" href="#head中常见的标签" aria-hidden="true">#</a> head中常见的标签</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;title&gt;&lt;/title&gt;:文档标题，显示在浏览器的左上方
&lt;meta/&gt;:元数据元素，用于定义网页的基本信息，比如文档的字符编码，查询关键字，刷新速度
&lt;style&gt;&lt;/style&gt;:定义文档样式
&lt;link/&gt;:引入外部css样式
&lt;script&gt;&lt;/script&gt;:引入外部脚本文件
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="文本元素" tabindex="-1"><a class="header-anchor" href="#文本元素" aria-hidden="true">#</a> 文本元素</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;h1&gt;到&lt;h6&gt;:文本标题元素，当页面上的文本需要突出显示时，可以使用标题元素
&lt;p&gt;:段落，当页面上的一段文本需要单独使用一个段落时，并于前后文本隔开，可以使用p标记
&lt;br/&gt;:换行标记
&lt;hr/&gt;:水平线，常用于页面上为大量的文档进行分割
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>分组元素:span 与 div
span:行内元素，不影响原有元素的布局，原先是一行，现在还是一行
div:块级元素，它会自成一行，单独占一段落，前后元素会被顶开，变成独立的行
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>em    :强调        -&gt; 默认是斜体
&lt;em&gt;强调&lt;/em&gt;

strong:更强烈的强调 -&gt; 粗体
&lt;strong&gt;强烈的强调&lt;/strong&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;q&gt;短引用文本&lt;/q&gt;:短引用文本,一句话如果引用一句经典语录,设置到此标签中
&lt;blockquote&gt; 长引用 &lt;/blockquote&gt; :长引用,一句话如果引用多句经典语录,设置到此标签中
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="图片和链接" tabindex="-1"><a class="header-anchor" href="#图片和链接" aria-hidden="true">#</a> 图片和链接</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>1.&lt;img src=&quot;图像的路径&quot; width=&quot;宽&quot; height=&quot;高&quot;/&gt;
注:相对路径(不以“/”开头的路径称为相对路径)以及绝对路径，不能使用物理地址

2.超级链接:点击，去往其他资源(页面)
&lt;a href=&quot;url&quot; target=&quot;&quot;&gt;文字或图片&lt;/a&gt;
target取值:_self(缺省值)，替换当前窗口的内容,有前后流程操作的时候使用，比如支付
           _blank,打开新的空白页面，显示内容，查看某个商品详情

3.锚点:当前页面的不同位置之间的跳转
第一步:使用a在目标位置定义一个锚点
      &lt;a name=&quot;锚点名称&quot;&gt;&lt;/a&gt;
第二步:使用a标签，配合href=“#锚点名称”执行具体锚点位置
      &lt;a href=&quot;#link1&quot;&gt;&lt;/a&gt;
      
4.直接回到页面顶端
简化写法:&lt;a href=&quot;#&quot;&gt;to top&lt;/a&gt;

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="列表元素" tabindex="-1"><a class="header-anchor" href="#列表元素" aria-hidden="true">#</a> 列表元素</h4><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code>什么是列表：将几项相同的内容排列在一起
所有列表都有列表类型和列表项组成<span class="token operator">:</span>
<span class="token operator">--</span>列表类型<span class="token operator">:</span>有序列表<span class="token generics"><span class="token punctuation">&lt;</span>ol<span class="token punctuation">&gt;</span></span>和无序列表<span class="token generics"><span class="token punctuation">&lt;</span>ul<span class="token punctuation">&gt;</span></span>
   <span class="token operator">--</span>ol<span class="token operator">:</span>type<span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">,</span><span class="token class-name">A</span><span class="token punctuation">,</span>a<span class="token punctuation">,</span><span class="token class-name">I</span><span class="token punctuation">,</span>i<span class="token punctuation">]</span>
   <span class="token operator">--</span>ul<span class="token operator">:</span>type<span class="token punctuation">[</span><span class="token function">circle</span><span class="token punctuation">(</span>空心圆<span class="token punctuation">)</span><span class="token punctuation">,</span><span class="token function">disc</span><span class="token punctuation">(</span>实心圆，缺省值<span class="token punctuation">)</span><span class="token punctuation">,</span><span class="token function">square</span><span class="token punctuation">(</span>方块<span class="token punctuation">)</span><span class="token punctuation">]</span>
<span class="token operator">--</span>列表项<span class="token operator">:</span><span class="token generics"><span class="token punctuation">&lt;</span>li<span class="token punctuation">&gt;</span></span><span class="token punctuation">,</span>用于指示具体的列表内容
<span class="token operator">--</span>列表嵌套<span class="token operator">:</span>
  <span class="token generics"><span class="token punctuation">&lt;</span>ul<span class="token punctuation">&gt;</span></span>
  	<span class="token generics"><span class="token punctuation">&lt;</span>li<span class="token punctuation">&gt;</span></span>one
    	<span class="token generics"><span class="token punctuation">&lt;</span>ol<span class="token punctuation">&gt;</span></span>
          <span class="token generics"><span class="token punctuation">&lt;</span>li<span class="token punctuation">&gt;</span></span><span class="token class-name">A</span><span class="token operator">&lt;</span><span class="token operator">/</span>li<span class="token operator">&gt;</span>
        <span class="token operator">&lt;</span><span class="token operator">/</span>ol<span class="token operator">&gt;</span>  
    <span class="token operator">&lt;</span><span class="token operator">/</span>li<span class="token operator">&gt;</span>    
    <span class="token generics"><span class="token punctuation">&lt;</span>li<span class="token punctuation">&gt;</span></span>two<span class="token operator">&lt;</span><span class="token operator">/</span>li<span class="token operator">&gt;</span>   
    <span class="token generics"><span class="token punctuation">&lt;</span>li<span class="token punctuation">&gt;</span></span>three<span class="token operator">&lt;</span><span class="token operator">/</span>li<span class="token operator">&gt;</span>   
  <span class="token operator">&lt;</span><span class="token operator">/</span>ul<span class="token operator">&gt;</span>
   
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="表格" tabindex="-1"><a class="header-anchor" href="#表格" aria-hidden="true">#</a> 表格</h4><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code>显示网格数据，也可以用于布局
<span class="token number">1.</span>创建表格<span class="token operator">:</span>
  <span class="token function">table</span><span class="token punctuation">(</span>定义表格<span class="token punctuation">)</span>  <span class="token function">tr</span><span class="token punctuation">(</span>创建行<span class="token punctuation">)</span>  <span class="token function">td</span><span class="token punctuation">(</span>创建列<span class="token punctuation">)</span>
<span class="token number">2.</span>常用属性
  table<span class="token operator">:</span>
  <span class="token operator">--</span>border定义表格边框线的宽度
  <span class="token operator">--</span>width<span class="token operator">/</span>height
  <span class="token operator">--</span>align<span class="token operator">:</span>定义表格水平方向的位置
  <span class="token operator">--</span>cellpadding<span class="token operator">:</span>单元格边框与内容之间的间距
  <span class="token operator">--</span>cellspacing<span class="token operator">:</span>单元格与单元格之间的距离
    
  tr<span class="token operator">:</span>
  <span class="token function">align</span><span class="token punctuation">(</span>水平方向位置left center right<span class="token punctuation">)</span><span class="token operator">/</span><span class="token function">valign</span><span class="token punctuation">(</span>垂直方向位置<span class="token operator">:</span>top center bottom<span class="token punctuation">)</span>
      
  td<span class="token operator">:</span>
  align<span class="token operator">/</span>valign<span class="token operator">/</span><span class="token function">colspan</span><span class="token punctuation">(</span>跨列<span class="token punctuation">)</span><span class="token operator">/</span><span class="token function">rowspan</span><span class="token punctuation">(</span>跨行<span class="token punctuation">)</span><span class="token operator">/</span>width<span class="token operator">/</span>height
      
<span class="token number">3.</span>行分组
  表格可以划分为<span class="token number">3</span>个部分<span class="token operator">:</span>表头，表主体和表尾
  表头行分组<span class="token operator">:</span><span class="token generics"><span class="token punctuation">&lt;</span>thead<span class="token punctuation">&gt;</span></span><span class="token operator">&lt;</span><span class="token operator">/</span>thead<span class="token operator">&gt;</span>
  表主体行分组<span class="token operator">:</span><span class="token generics"><span class="token punctuation">&lt;</span>tbody<span class="token punctuation">&gt;</span></span><span class="token operator">&lt;</span><span class="token operator">/</span>tbody<span class="token operator">&gt;</span>
  表尾行分组<span class="token operator">:</span><span class="token generics"><span class="token punctuation">&lt;</span>tfoot<span class="token punctuation">&gt;</span></span><span class="token operator">&lt;</span><span class="token operator">/</span>tfoot<span class="token operator">&gt;</span>
 <span class="token operator">--</span>由上而下 从左到右
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="表单" tabindex="-1"><a class="header-anchor" href="#表单" aria-hidden="true">#</a> 表单</h4><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code>表单用于显示，收集信息，并将信息提交到服务器。
定义表单<span class="token operator">:</span>使用成对的<span class="token generics"><span class="token punctuation">&lt;</span>form<span class="token punctuation">&gt;</span></span><span class="token operator">&lt;</span><span class="token operator">/</span>form<span class="token operator">&gt;</span>标记，表示要将此元素中所涵盖的控制中的数据传输给服务器。
主要属性<span class="token operator">:</span>
  action<span class="token operator">:</span>表单提交的地址
  method<span class="token operator">:</span>设置表单数据提交的方式<span class="token function">get</span><span class="token punctuation">(</span>默认值<span class="token punctuation">)</span><span class="token operator">/</span>post
  enctype<span class="token operator">:</span>设置表单数据进行编码的方式，默认application<span class="token operator">/</span>x<span class="token operator">-</span>www<span class="token operator">-</span>form<span class="token operator">-</span>urlencoded<span class="token punctuation">,</span>当表单要传数据时，enctype<span class="token operator">=</span><span class="token string">&quot;multipart/form-data&quot;</span>
    

表单可以包含不同类型的表单控件，包括<span class="token operator">:</span>
<span class="token operator">--</span>input元素
  文本框，密码框，单选框，复选框，按钮，隐藏框，文件选择框
<span class="token operator">--</span>其他元素
  标签
  <span class="token operator">--</span><span class="token operator">&lt;</span>label <span class="token keyword">for</span><span class="token operator">=</span><span class="token string">&quot;需要关联的控件的ID&quot;</span><span class="token operator">&gt;</span>文本<span class="token operator">&lt;</span><span class="token operator">/</span>label<span class="token operator">&gt;</span>
  文本域  
  <span class="token operator">--</span><span class="token operator">&lt;</span>textarea cols<span class="token operator">=</span><span class="token string">&quot;指定文本域的列数&quot;</span> rows<span class="token operator">=</span><span class="token string">&quot;指定文本域的行数&quot;</span> readonly<span class="token operator">&gt;</span>文本<span class="token operator">&lt;</span><span class="token operator">/</span>textarea<span class="token operator">&gt;</span>
  下拉框  
  <span class="token operator">--</span><span class="token generics"><span class="token punctuation">&lt;</span>select<span class="token punctuation">&gt;</span></span>
    	<span class="token operator">&lt;</span>option value<span class="token operator">=</span><span class="token string">&quot;1&quot;</span><span class="token operator">&gt;</span>java<span class="token operator">&lt;</span><span class="token operator">/</span>option<span class="token operator">&gt;</span>
        <span class="token operator">&lt;</span>option value<span class="token operator">=</span><span class="token string">&quot;2&quot;</span><span class="token operator">&gt;</span>uid<span class="token operator">&lt;</span><span class="token operator">/</span>option<span class="token operator">&gt;</span>
    <span class="token operator">&lt;</span><span class="token operator">/</span>select<span class="token operator">&gt;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="iframe" tabindex="-1"><a class="header-anchor" href="#iframe" aria-hidden="true">#</a> iframe</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>在当前页面引入并显示其他页面(嵌入)，常用于类似广告页面的嵌入
语法:&lt;iframe src=&quot;页面路径&quot;&gt;&lt;/iframe&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="代码" tabindex="-1"><a class="header-anchor" href="#代码" aria-hidden="true">#</a> 代码</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>添加一行代码,一般计算机代码放在这个标签中

&lt;code&gt;一行代码&lt;/code&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>添加一段代码

&lt;pre&gt;多行代码&lt;/pre&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,33),p=[l];function o(i,c){return s(),a("div",null,p)}const d=n(t,[["render",o],["__file","01html.html.vue"]]);export{d as default};
