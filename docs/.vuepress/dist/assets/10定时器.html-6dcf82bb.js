import{_ as e,p as i,q as n,a1 as a}from"./framework-7db056f4.js";const r={},d=a(`<p>理论基础：</p><p>小顶堆：</p><pre><code>堆是一个完全二叉树。

  完全二叉树：除了最后一层，都达到最大节点数，且最后一层节点都靠左排列。

根节点最大的堆叫做大顶堆，根节点最小的堆叫做小顶堆。
</code></pre><p>时间轮算法：</p><h4 id="jdk定时器timer" tabindex="-1"><a class="header-anchor" href="#jdk定时器timer" aria-hidden="true">#</a> JDK定时器Timer</h4><p>Timer是一个定时器工具，用来执行指定的任务</p><p>TimerTask: 抽象类</p><p>delay(开始执行的等待时间)</p><p>Timer是一种工具，线程用其安排以后在后台线程中执行的任务。可安排任务执行一次，或者定期重复执行。实际上是个线程，定时调度所拥有的TimerTasks。</p><p>TimerTask是一个抽象类，它的子类由 Timer 安排为一次执行或重复执行的任务。实际上就是一个拥有run方法的类，需要定时执行的代码放到run方法体内。</p><div class="language-Java line-numbers-mode" data-ext="Java"><pre class="language-Java"><code>package com.xxx.test1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TimerTask;

public class MyTimerTask extends TimerTask {

    private int num = 1;

    @Override
    public void run() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(&quot;1.txt&quot;));
            bw.write(num+&quot;&quot;);
            num++;
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-Java line-numbers-mode" data-ext="Java"><pre class="language-Java"><code>package com.xxx.test1;

import java.util.Timer;

public class Exe {
    public static void main(String[] args) {

        Timer timer = new Timer();
        MyTimerTask task = new MyTimerTask();

        timer.schedule(task,1000,1000);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="定时任务线程池" tabindex="-1"><a class="header-anchor" href="#定时任务线程池" aria-hidden="true">#</a> 定时任务线程池</h4><p>指放入线程池的任务，可以按照指定的等待周期循环执行。</p><p>Java里面<code>ScheduledThreadPoolExecutor</code>这个类实现了这种功能。 Spring里面的定时任务也是在<code>ScheduledThreadPoolExecutor</code>的基础上扩展而来。</p><h4 id="定时任务框架-quartz" tabindex="-1"><a class="header-anchor" href="#定时任务框架-quartz" aria-hidden="true">#</a> 定时任务框架-quartz</h4><p>是一个开源项目，完全由Java开发，可以用来执行定时任务，类似于java.util.Timer。但是相较于Timer， Quartz增加了很多功能：</p><ul><li>持久性作业 - 就是保持调度定时的状态;</li><li>作业管理 - 对调度作业进行有效的管理;</li></ul><p><strong>Quartz的基本组成部分：</strong></p><ul><li>调度器：Scheduler</li><li>任务：JobDetail</li><li>触发器：Trigger，包括SimpleTrigger和CronTrigger</li></ul>`,20),l=[d];function s(v,c){return i(),n("div",null,l)}const t=e(r,[["render",s],["__file","10定时器.html.vue"]]);export{t as default};
