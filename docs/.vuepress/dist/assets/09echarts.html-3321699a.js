import{_ as l,M as t,p as h,q as c,R as e,t as n,N as i,V as s,a1 as r}from"./framework-7db056f4.js";const o={},v=e("h2",{id:"入门篇",tabindex:"-1"},[e("a",{class:"header-anchor",href:"#入门篇","aria-hidden":"true"},"#"),n(" 入门篇")],-1),m={id:"获取-apache-echarts",tabindex:"-1"},u=e("a",{class:"header-anchor",href:"#获取-apache-echarts","aria-hidden":"true"},"#",-1),b={href:"https://echarts.apache.org/handbook/zh/get-started/",target:"_blank",rel:"noopener noreferrer"},p=r('<p>Apache ECharts 提供了多种安装方式，你可以根据项目的实际情况选择以下任意一种方式安装。</p><ul><li>从 GitHub 获取</li><li>从 npm 获取</li><li>从 CDN 获取</li><li>在线定制</li></ul><p>接下来我们将分别介绍这些安装方式，以及下载后的目录结构。</p><h4 id="安装方式" tabindex="-1"><a class="header-anchor" href="#安装方式" aria-hidden="true">#</a> 安装方式</h4><h5 id="从-npm-获取" tabindex="-1"><a class="header-anchor" href="#从-npm-获取" aria-hidden="true">#</a> 从 npm 获取</h5><p><code>npm install echarts</code> 详见在项目中引入 Apache ECharts。</p><h5 id="从-cdn-获取" tabindex="-1"><a class="header-anchor" href="#从-cdn-获取" aria-hidden="true">#</a> 从 CDN 获取</h5><p>可以从以下免费 CDN 中获取和引用 ECharts。</p><ul><li>jsDelivr</li><li>unpkg</li><li>cdnjs</li></ul><h5 id="从-github-获取" tabindex="-1"><a class="header-anchor" href="#从-github-获取" aria-hidden="true">#</a> 从 GitHub 获取</h5>',10),_={href:"https://github.com/apache/echarts/releases",target:"_blank",rel:"noopener noreferrer"},C=e("code",null,"dist",-1),f=e("code",null,"echarts.js",-1),x=r(`<h5 id="在线定制" tabindex="-1"><a class="header-anchor" href="#在线定制" aria-hidden="true">#</a> 在线定制</h5><p>如果只想引入部分模块以减少包体积，可以使用 ECharts 在线定制功能。</p><h3 id="在项目中引入-apache-echarts" tabindex="-1"><a class="header-anchor" href="#在项目中引入-apache-echarts" aria-hidden="true">#</a> 在项目中引入 Apache ECharts</h3><p>假如你的开发环境使用了 <code>npm</code> 或者 <code>yarn</code> 等包管理工具，并且使用 <code>webpack</code> 等打包工具进行构建，本文将会介绍如何引入 Apache EChartsTM 并通过 tree-shaking 特性只打包需要的模块以减少包体积。</p><h4 id="npm-安装-echarts" tabindex="-1"><a class="header-anchor" href="#npm-安装-echarts" aria-hidden="true">#</a> NPM 安装 ECharts</h4><p>你可以使用如下命令通过 npm 安装 ECharts</p><p><code>npm install echarts --save</code></p><h4 id="引入-echarts" tabindex="-1"><a class="header-anchor" href="#引入-echarts" aria-hidden="true">#</a> 引入 ECharts</h4><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>import * as echarts from &#39;echarts&#39;;

// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById(&#39;main&#39;));
// 绘制图表
myChart.setOption({
  title: {
    text: &#39;ECharts 入门示例&#39;
  },
  tooltip: {},
  xAxis: {
    data: [&#39;衬衫&#39;, &#39;羊毛衫&#39;, &#39;雪纺衫&#39;, &#39;裤子&#39;, &#39;高跟鞋&#39;, &#39;袜子&#39;]
  },
  yAxis: {},
  series: [
    {
      name: &#39;销量&#39;,
      type: &#39;bar&#39;,
      data: [5, 20, 36, 10, 10, 20]
    }
  ]
});
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="按需引入-echarts-图表和组件" tabindex="-1"><a class="header-anchor" href="#按需引入-echarts-图表和组件" aria-hidden="true">#</a> 按需引入 ECharts 图表和组件</h4><p>上面的代码会引入 ECharts 中所有的图表和组件，如果你不想引入所有组件，也可以使用 ECharts 提供的按需引入的接口来打包必须的组件。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>// 引入 echarts 核心模块，核心模块提供了 echarts 使用必须要的接口。
import * as echarts from &#39;echarts/core&#39;;
// 引入柱状图图表，图表后缀都为 Chart
import { BarChart } from &#39;echarts/charts&#39;;
// 引入提示框，标题，直角坐标系，数据集，内置数据转换器组件，组件后缀都为 Component
import {
  TitleComponent,
  TooltipComponent,
  GridComponent,
  DatasetComponent,
  TransformComponent
} from &#39;echarts/components&#39;;
// 标签自动布局、全局过渡动画等特性
import { LabelLayout, UniversalTransition } from &#39;echarts/features&#39;;
// 引入 Canvas 渲染器，注意引入 CanvasRenderer 或者 SVGRenderer 是必须的一步
import { CanvasRenderer } from &#39;echarts/renderers&#39;;

// 注册必须的组件
echarts.use([
  TitleComponent,
  TooltipComponent,
  GridComponent,
  DatasetComponent,
  TransformComponent,
  BarChart,
  LabelLayout,
  UniversalTransition,
  CanvasRenderer
]);

// 接下来的使用就跟之前一样，初始化图表，设置配置项
var myChart = echarts.init(document.getElementById(&#39;main&#39;));
myChart.setOption({
  // ...
});
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>需要注意的是为了保证打包的体积是最小的，ECharts 按需引入的时候不再提供任何渲染器，所以需要选择引入 CanvasRenderer 或者 SVGRenderer 作为渲染器。这样的好处是假如你只需要使用 svg 渲染模式，打包的结果中就不会再包含无需使用的 CanvasRenderer 模块。</p></blockquote><p>我们在示例编辑页的“完整代码”标签提供了非常方便的生成按需引入代码的功能。这个功能会根据当前的配置项动态生成最小的按需引入的代码。你可以直接在你的项目中使用。</p><h4 id="在-typescript-中按需引入" tabindex="-1"><a class="header-anchor" href="#在-typescript-中按需引入" aria-hidden="true">#</a> 在 TypeScript 中按需引入</h4><p>对于使用了 TypeScript 来开发 ECharts 的开发者，我们提供了类型接口来组合出最小的 EChartsOption 类型。这个更严格的类型可以有效帮助你检查出是否少加载了组件或者图表。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>import * as echarts from &#39;echarts/core&#39;;
import {
  BarChart,
  // 系列类型的定义后缀都为 SeriesOption
  BarSeriesOption,
  LineChart,
  LineSeriesOption
} from &#39;echarts/charts&#39;;
import {
  TitleComponent,
  // 组件类型的定义后缀都为 ComponentOption
  TitleComponentOption,
  TooltipComponent,
  TooltipComponentOption,
  GridComponent,
  GridComponentOption,
  // 数据集组件
  DatasetComponent,
  DatasetComponentOption,
  // 内置数据转换器组件 (filter, sort)
  TransformComponent
} from &#39;echarts/components&#39;;
import { LabelLayout, UniversalTransition } from &#39;echarts/features&#39;;
import { CanvasRenderer } from &#39;echarts/renderers&#39;;

// 通过 ComposeOption 来组合出一个只有必须组件和图表的 Option 类型
type ECOption = echarts.ComposeOption&lt;
  | BarSeriesOption
  | LineSeriesOption
  | TitleComponentOption
  | TooltipComponentOption
  | GridComponentOption
  | DatasetComponentOption
&gt;;

// 注册必须的组件
echarts.use([
  TitleComponent,
  TooltipComponent,
  GridComponent,
  DatasetComponent,
  TransformComponent,
  BarChart,
  LineChart,
  LabelLayout,
  UniversalTransition,
  CanvasRenderer
]);

const option: ECOption = {
  // ...
};
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="概念篇" tabindex="-1"><a class="header-anchor" href="#概念篇" aria-hidden="true">#</a> 概念篇</h2><h2 id="应用篇" tabindex="-1"><a class="header-anchor" href="#应用篇" aria-hidden="true">#</a> 应用篇</h2><h3 id="_1-柱状图" tabindex="-1"><a class="header-anchor" href="#_1-柱状图" aria-hidden="true">#</a> 1.柱状图</h3>`,20),g={id:"_1-1-基础柱状图",tabindex:"-1"},E=e("a",{class:"header-anchor",href:"#_1-1-基础柱状图","aria-hidden":"true"},"#",-1),T=r('<h4 id="_1-2-堆叠柱状图" tabindex="-1"><a class="header-anchor" href="#_1-2-堆叠柱状图" aria-hidden="true">#</a> 1.2 堆叠柱状图</h4><h4 id="_1-3-动态排序柱状图" tabindex="-1"><a class="header-anchor" href="#_1-3-动态排序柱状图" aria-hidden="true">#</a> 1.3 动态排序柱状图</h4><h4 id="_1-4-动态排序柱状图" tabindex="-1"><a class="header-anchor" href="#_1-4-动态排序柱状图" aria-hidden="true">#</a> 1.4 动态排序柱状图</h4><h4 id="_1-5-阶梯瀑布图" tabindex="-1"><a class="header-anchor" href="#_1-5-阶梯瀑布图" aria-hidden="true">#</a> 1.5 阶梯瀑布图</h4><h3 id="_2-折线图" tabindex="-1"><a class="header-anchor" href="#_2-折线图" aria-hidden="true">#</a> 2.折线图</h3>',5),O={id:"_2-1-基础折线图",tabindex:"-1"},y=e("a",{class:"header-anchor",href:"#_2-1-基础折线图","aria-hidden":"true"},"#",-1),k=r('<h4 id="_2-2-堆叠折线图" tabindex="-1"><a class="header-anchor" href="#_2-2-堆叠折线图" aria-hidden="true">#</a> 2.2 堆叠折线图</h4><h4 id="_2-3-区域折线图" tabindex="-1"><a class="header-anchor" href="#_2-3-区域折线图" aria-hidden="true">#</a> 2.3 区域折线图</h4><h4 id="_2-4-平滑折线图" tabindex="-1"><a class="header-anchor" href="#_2-4-平滑折线图" aria-hidden="true">#</a> 2.4 平滑折线图</h4><h4 id="_2-5-阶梯线图" tabindex="-1"><a class="header-anchor" href="#_2-5-阶梯线图" aria-hidden="true">#</a> 2.5 阶梯线图</h4><h3 id="_3-饼图" tabindex="-1"><a class="header-anchor" href="#_3-饼图" aria-hidden="true">#</a> 3.饼图</h3>',5),L={id:"_3-1-基础饼图",tabindex:"-1"},R=e("a",{class:"header-anchor",href:"#_3-1-基础饼图","aria-hidden":"true"},"#",-1),B=r('<h4 id="_3-2-圆环图" tabindex="-1"><a class="header-anchor" href="#_3-2-圆环图" aria-hidden="true">#</a> 3.2 圆环图</h4><h4 id="_3-3-南丁格尔图-玫瑰图" tabindex="-1"><a class="header-anchor" href="#_3-3-南丁格尔图-玫瑰图" aria-hidden="true">#</a> 3.3 南丁格尔图（玫瑰图）</h4><h3 id="_4-散点图" tabindex="-1"><a class="header-anchor" href="#_4-散点图" aria-hidden="true">#</a> 4.散点图</h3>',3),S={id:"_4-1-基础散点图",tabindex:"-1"},D=e("a",{class:"header-anchor",href:"#_4-1-基础散点图","aria-hidden":"true"},"#",-1),G=e("h3",{id:"跨平台方案",tabindex:"-1"},[e("a",{class:"header-anchor",href:"#跨平台方案","aria-hidden":"true"},"#"),n(" 跨平台方案")],-1),N={id:"_1-服务端渲染",tabindex:"-1"},w=e("a",{class:"header-anchor",href:"#_1-服务端渲染","aria-hidden":"true"},"#",-1),A={id:"_2-微信小程序",tabindex:"-1"},V=e("a",{class:"header-anchor",href:"#_2-微信小程序","aria-hidden":"true"},"#",-1),I=e("h4",{id:"_3-百度智能小程序",tabindex:"-1"},[e("a",{class:"header-anchor",href:"#_3-百度智能小程序","aria-hidden":"true"},"#"),n(" 3 百度智能小程序")],-1);function U(j,q){const d=t("ExternalLinkIcon"),a=t("RouterLink");return h(),c("div",null,[v,e("h3",m,[u,n(" 获取 "),e("a",b,[n("Apache ECharts"),i(d)])]),p,e("p",null,[n("apache/echarts 项目的 "),e("a",_,[n("release"),i(d)]),n(" 页面可以找到各个版本的链接。点击下载页面下方 Assets 中的 Source code，解压后 "),C,n(" 目录下的 "),f,n(" 即为包含完整 ECharts 功能的文件。")]),x,e("h4",g,[E,n(),i(a,{to:"/web/09-11.html"},{default:s(()=>[n("1.1 基础柱状图")]),_:1})]),T,e("h4",O,[y,n(),i(a,{to:"/web/09-21.html"},{default:s(()=>[n("2.1 基础折线图")]),_:1})]),k,e("h4",L,[R,n(),i(a,{to:"/web/09-31.html"},{default:s(()=>[n("3.1 基础饼图")]),_:1})]),B,e("h4",S,[D,n(),i(a,{to:"/web/09-41.html"},{default:s(()=>[n("4.1 基础散点图")]),_:1})]),G,e("h4",N,[w,n(" 1 "),i(a,{to:"/web/09-k1.html"},{default:s(()=>[n("服务端渲染")]),_:1})]),e("h4",A,[V,n(" 2 "),i(a,{to:"/web/09-k2.html"},{default:s(()=>[n("微信小程序")]),_:1})]),I])}const H=l(o,[["render",U],["__file","09echarts.html.vue"]]);export{H as default};
