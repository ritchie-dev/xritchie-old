import{_ as e,M as t,p,q as l,R as n,t as s,N as c,a1 as i}from"./framework-7db056f4.js";const o={},u=n("p",null,"基于NodeJS，CSS预处理器",-1),r=n("h3",{id:"_1-简介",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#_1-简介","aria-hidden":"true"},"#"),s(" 1.简介")],-1),d=n("p",null,"并不是新语言，只是让CSS更高效，换了一种写法。",-1),k={href:"https://less.bootcss.com/",target:"_blank",rel:"noopener noreferrer"},v=i(`<p>Less（代表Leaner样式表）是CSS的向后兼容语言扩展。</p><h3 id="_2-引入" tabindex="-1"><a class="header-anchor" href="#_2-引入" aria-hidden="true">#</a> 2.引入</h3><blockquote><ul><li>与Node.js一起使用：</li></ul><div class="language-less line-numbers-mode" data-ext="less"><pre class="language-less"><code>npm install <span class="token operator">-</span>g less
&gt; lessc styles.less styles.css
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>或浏览器：</li></ul><div class="language-html line-numbers-mode" data-ext="html"><pre class="language-html"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>link</span> <span class="token attr-name">rel</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>stylesheet/less<span class="token punctuation">&quot;</span></span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>text/css<span class="token punctuation">&quot;</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>xxx/styles.less<span class="token punctuation">&quot;</span></span> <span class="token punctuation">/&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>script</span> <span class="token attr-name">src</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>//cdnjs.cloudflare.com/ajax/libs/less.js/3.11.1/less.min.js<span class="token punctuation">&quot;</span></span> <span class="token punctuation">&gt;</span></span><span class="token script"></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>script</span><span class="token punctuation">&gt;</span></span>
<span class="token comment">&lt;!-- 
     注意：link标签的href连接less文件相对地址
    --&gt;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></blockquote><h3 id="_3-注释" tabindex="-1"><a class="header-anchor" href="#_3-注释" aria-hidden="true">#</a> 3.注释</h3><div class="language-html line-numbers-mode" data-ext="html"><pre class="language-html"><code>//这种注释，编译后不会出现在CSS文件上

/*
 * 这种注释，编译后会出现在CSS文件上
 */
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="_4-编译" tabindex="-1"><a class="header-anchor" href="#_4-编译" aria-hidden="true">#</a> 4.编译</h3><blockquote><p>1.用第三方编译工具</p><p>Koala网址http://koala-app.com/</p><p>国人开发，可以把less编译成css。</p><p>2.用插件编译</p><p>VSCode上的easyLess</p><p>Ctrl+S即可生成css文件。</p><p>3.HBuilderX上也有Less插件，直接选中lss文件鼠标右键即可。</p></blockquote><blockquote><p>用插件之后head就不用再引入less了，改为引入css即可：</p><div class="language-html line-numbers-mode" data-ext="html"><pre class="language-html"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>link</span> <span class="token attr-name">rel</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>stylesheet<span class="token punctuation">&quot;</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>less/test01.css<span class="token punctuation">&quot;</span></span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>href是css文件的相对位置</p></blockquote><h3 id="_5-变量" tabindex="-1"><a class="header-anchor" href="#_5-变量" aria-hidden="true">#</a> 5.变量</h3><blockquote><p>@变量名:值</p><p>用变量去定义一个属性名，名字要加{}，eg. @{bgc}：red;</p><p>@imgurl:&#39;../xx/xxx/images&#39;; //定义图片全路径，要加引号&#39;&#39;</p><p>image:url(&#39;@{imgurl}/01.jpg&#39;); //路径名要加{}</p></blockquote><h3 id="_6-混合" tabindex="-1"><a class="header-anchor" href="#_6-混合" aria-hidden="true">#</a> 6.混合</h3><blockquote><p>Bbox可以通过.Abox的方式引入Abox的样式，这样就可以同一样式复用。</p><p>样式可以加参数，不写的话会默认复用。</p><p>混合可以多参数</p><p>后定义的样式会覆盖旧的样式</p></blockquote><div class="language-less line-numbers-mode" data-ext="less"><pre class="language-less"><code><span class="token variable">@w<span class="token punctuation">:</span></span>100px<span class="token punctuation">;</span>
<span class="token variable">@h<span class="token punctuation">:</span></span>120px<span class="token punctuation">;</span>

<span class="token selector">.bg(<span class="token variable">@bg</span>)</span><span class="token punctuation">{</span>
    <span class="token property">background-color</span><span class="token punctuation">:</span> <span class="token variable">@bg</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.box1</span><span class="token punctuation">{</span>
    <span class="token property">width</span><span class="token punctuation">:</span> <span class="token variable">@w</span><span class="token punctuation">;</span>
    <span class="token property">height</span><span class="token punctuation">:</span> <span class="token variable">@h</span><span class="token punctuation">;</span>
    <span class="token mixin-usage function">.bg</span><span class="token punctuation">(</span>coral<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.bd(<span class="token variable">@w</span>:5px)</span><span class="token punctuation">{</span>
    <span class="token property">border</span><span class="token punctuation">:</span> <span class="token variable">@w</span> solid green<span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.box2</span><span class="token punctuation">{</span>
    <span class="token property">width</span><span class="token punctuation">:</span> <span class="token variable">@h</span><span class="token punctuation">;</span>
    <span class="token mixin-usage function">.bd</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.box3</span><span class="token punctuation">{</span>
    <span class="token property">height</span><span class="token punctuation">:</span> <span class="token variable">@w</span><span class="token punctuation">;</span>
    <span class="token mixin-usage function">.bd</span><span class="token punctuation">(</span>15px<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.box4</span><span class="token punctuation">{</span>
    <span class="token property">width</span><span class="token punctuation">:</span> 200px<span class="token punctuation">;</span>
    <span class="token property">height</span><span class="token punctuation">:</span> <span class="token variable">@h</span><span class="token punctuation">;</span>
    <span class="token mixin-usage function">.bg</span><span class="token punctuation">(</span>coral<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.box5</span><span class="token punctuation">{</span>
    <span class="token mixin-usage function">.box4</span><span class="token punctuation">;</span><span class="token comment">// 直接延用box4的样式</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="_7-匹配模式" tabindex="-1"><a class="header-anchor" href="#_7-匹配模式" aria-hidden="true">#</a> 7.匹配模式</h3><blockquote><p>定义一组样式，公有部分实现复用，调用哪个实现哪个样式。</p><p>@_</p></blockquote><div class="language-less line-numbers-mode" data-ext="less"><pre class="language-less"><code><span class="token selector">.box1</span><span class="token punctuation">{</span>
    <span class="token property">width</span><span class="token punctuation">:</span> 0px<span class="token punctuation">;</span>
    <span class="token property">height</span><span class="token punctuation">:</span> 0px<span class="token punctuation">;</span>
    <span class="token property">overflow</span><span class="token punctuation">:</span> hidden<span class="token punctuation">;</span><span class="token comment">// 不加的话背景文字会溢出</span>

    <span class="token property">border-width</span><span class="token punctuation">:</span> 10px<span class="token punctuation">;</span>
    <span class="token property">border-color</span><span class="token punctuation">:</span> transparent transparent red transparent<span class="token punctuation">;</span> <span class="token comment">//transparent透明</span>
    <span class="token property">border-style</span><span class="token punctuation">:</span> dashed dashed solid dashed<span class="token punctuation">;</span> <span class="token comment">//dashed虚线</span>
<span class="token punctuation">}</span>
<span class="token comment">// 匹配模式</span>
<span class="token comment">// 当满足up，right，down，left某个条件时，执行对应的代码块</span>
<span class="token selector">.triangle(up,<span class="token variable">@w</span>:10px,<span class="token variable">@c</span>:red,<span class="token variable">@s</span>:solid)</span> <span class="token punctuation">{</span><span class="token comment">//triangle三角形</span>
    <span class="token property">border-width</span><span class="token punctuation">:</span> <span class="token variable">@w</span><span class="token punctuation">;</span>
    <span class="token property">border-color</span><span class="token punctuation">:</span> transparent transparent <span class="token variable">@c</span> transparent<span class="token punctuation">;</span>
    <span class="token property">border-style</span><span class="token punctuation">:</span> dashed dashed <span class="token variable">@s</span> dashed<span class="token punctuation">;</span> 
<span class="token punctuation">}</span>
<span class="token selector">.triangle(right,<span class="token variable">@w</span>:10px,<span class="token variable">@c</span>:red,<span class="token variable">@s</span>:solid)</span> <span class="token punctuation">{</span>
    <span class="token property">border-width</span><span class="token punctuation">:</span> <span class="token variable">@w</span><span class="token punctuation">;</span>
    <span class="token property">border-color</span><span class="token punctuation">:</span> transparent transparent transparent <span class="token variable">@c</span><span class="token punctuation">;</span>
    <span class="token property">border-style</span><span class="token punctuation">:</span> dashed dashed dashed <span class="token variable">@s</span><span class="token punctuation">;</span> 
<span class="token punctuation">}</span>
<span class="token selector">.triangle(down,<span class="token variable">@w</span>:10px,<span class="token variable">@c</span>:red,<span class="token variable">@s</span>:solid)</span> <span class="token punctuation">{</span>
    <span class="token property">border-width</span><span class="token punctuation">:</span> <span class="token variable">@w</span><span class="token punctuation">;</span>
    <span class="token property">border-color</span><span class="token punctuation">:</span> <span class="token variable">@c</span> transparent transparent transparent<span class="token punctuation">;</span>
    <span class="token property">border-style</span><span class="token punctuation">:</span> <span class="token variable">@s</span> dashed dashed dashed<span class="token punctuation">;</span> 
<span class="token punctuation">}</span>
<span class="token selector">.triangle(left,<span class="token variable">@w</span>:10px,<span class="token variable">@c</span>:red,<span class="token variable">@s</span>:solid)</span> <span class="token punctuation">{</span>
    <span class="token property">border-width</span><span class="token punctuation">:</span> <span class="token variable">@w</span><span class="token punctuation">;</span>
    <span class="token property">border-color</span><span class="token punctuation">:</span> transparent <span class="token variable">@c</span> transparent transparent<span class="token punctuation">;</span>
    <span class="token property">border-style</span><span class="token punctuation">:</span> dashed <span class="token variable">@s</span> dashed dashed<span class="token punctuation">;</span> 
<span class="token punctuation">}</span>

<span class="token selector">.box2</span><span class="token punctuation">{</span>
    <span class="token property">width</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span>
    <span class="token property">height</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span>
    <span class="token property">overflow</span><span class="token punctuation">:</span> hidden<span class="token punctuation">;</span>

    <span class="token mixin-usage function">.triangle</span><span class="token punctuation">(</span>right<span class="token punctuation">,</span> 10px<span class="token punctuation">,</span> green<span class="token punctuation">,</span> solid<span class="token punctuation">)</span>
<span class="token punctuation">}</span>

<span class="token comment">// 进一步简化，讲公有样式再提取出来</span>
<span class="token comment">// 注意：这个@_是固定格式</span>
<span class="token selector">.triangle(<span class="token variable">@_</span>,<span class="token variable">@w</span>,<span class="token variable">@c</span>,solid)</span><span class="token punctuation">{</span>
    <span class="token property">width</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span>
    <span class="token property">height</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span>
    <span class="token property">overflow</span><span class="token punctuation">:</span> hidden<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token selector">.box3</span><span class="token punctuation">{</span>
    <span class="token mixin-usage function">.triangle</span><span class="token punctuation">(</span>left<span class="token punctuation">,</span> 20px<span class="token punctuation">,</span> coral<span class="token punctuation">,</span> solid<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-less line-numbers-mode" data-ext="less"><pre class="language-less"><code><span class="token selector">.pos(r)</span><span class="token punctuation">{</span>
    <span class="token property">position</span><span class="token punctuation">:</span> relative<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token selector">.pos(a)</span><span class="token punctuation">{</span>
    <span class="token property">position</span><span class="token punctuation">:</span> absolute<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token selector">.pos(f)</span><span class="token punctuation">{</span>
    <span class="token property">position</span><span class="token punctuation">:</span> fixed<span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token selector">.box4</span><span class="token punctuation">{</span>
    <span class="token mixin-usage function">.pos</span><span class="token punctuation">(</span>r<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token mixin-usage function">.pos</span><span class="token punctuation">(</span>a<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token mixin-usage function">.pos</span><span class="token punctuation">(</span>f<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token comment">// 后面的会将前面的覆盖掉</span>
    <span class="token property">left</span><span class="token punctuation">:</span> 10px<span class="token punctuation">;</span>
    <span class="token property">top</span><span class="token punctuation">:</span> 20px<span class="token punctuation">;</span>
    <span class="token property">width</span><span class="token punctuation">:</span> 100px<span class="token punctuation">;</span>
    <span class="token property">height</span><span class="token punctuation">:</span> 200px<span class="token punctuation">;</span>
    <span class="token property">background</span><span class="token punctuation">:</span> coral<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="_8-嵌套" tabindex="-1"><a class="header-anchor" href="#_8-嵌套" aria-hidden="true">#</a> 8.嵌套</h3>`,18);function b(m,h){const a=t("ExternalLinkIcon");return p(),l("div",null,[u,r,d,n("p",null,[n("a",k,[s("中文版网页"),c(a)])]),v])}const x=e(o,[["render",b],["__file","13less.html.vue"]]);export{x as default};
