import{_ as s,p as n,q as a,a1 as e}from"./framework-7db056f4.js";const i={},l=e(`<h2 id="if语句" tabindex="-1"><a class="header-anchor" href="#if语句" aria-hidden="true">#</a> if语句</h2><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">if</span><span class="token punctuation">(</span>布尔表达式<span class="token punctuation">)</span>
<span class="token punctuation">{</span>
   <span class="token comment">//如果布尔表达式为true将执行的语句</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="if-else语句" tabindex="-1"><a class="header-anchor" href="#if-else语句" aria-hidden="true">#</a> if...else语句</h2><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">if</span><span class="token punctuation">(</span>布尔表达式<span class="token punctuation">)</span><span class="token punctuation">{</span>
   <span class="token comment">//如果布尔表达式的值为true</span>
<span class="token punctuation">}</span><span class="token keyword">else</span><span class="token punctuation">{</span>
   <span class="token comment">//如果布尔表达式的值为false</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="if-else-if-else-语句" tabindex="-1"><a class="header-anchor" href="#if-else-if-else-语句" aria-hidden="true">#</a> if...else if...else 语句</h2><p>if 语句后面可以跟 else if…else 语句，这种语句可以检测到多种可能的情况。</p><p>使用 if，else if，else 语句的时候，需要注意下面几点：</p><ul><li>if 语句至多有 1 个 else 语句，else 语句在所有的 else if 语句之后。</li><li>if 语句可以有若干个 else if 语句，它们必须在 else 语句之前。</li><li>一旦其中一个 else if 语句检测为 true，其他的 else if 以及 else 语句都将跳过执行。</li></ul><p>嵌套的 if…else 语句 使用嵌套的 if…else 语句是合法的。也就是说你可以在另一个 if 或者 else if 语句中使用 if 或者 else if 语句。</p><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">if</span><span class="token punctuation">(</span>布尔表达式 <span class="token number">1</span><span class="token punctuation">)</span><span class="token punctuation">{</span>
   <span class="token comment">////如果布尔表达式 1的值为true执行代码</span>
   <span class="token keyword">if</span><span class="token punctuation">(</span>布尔表达式 <span class="token number">2</span><span class="token punctuation">)</span><span class="token punctuation">{</span>
      <span class="token comment">////如果布尔表达式 2的值为true执行代码</span>
   <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="switch-case-语句" tabindex="-1"><a class="header-anchor" href="#switch-case-语句" aria-hidden="true">#</a> switch case 语句</h2><p>判断一个变量与一系列值中某个值是否相等，每个值称为一个分支。</p><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">switch</span><span class="token punctuation">(</span>expression<span class="token punctuation">)</span><span class="token punctuation">{</span>
    <span class="token keyword">case</span> value <span class="token operator">:</span>
       <span class="token comment">//语句</span>
       <span class="token keyword">break</span><span class="token punctuation">;</span> <span class="token comment">//可选</span>
    <span class="token keyword">case</span> value <span class="token operator">:</span>
       <span class="token comment">//语句</span>
       <span class="token keyword">break</span><span class="token punctuation">;</span> <span class="token comment">//可选</span>
    <span class="token comment">//你可以有任意数量的case语句</span>
    <span class="token keyword">default</span> <span class="token operator">:</span> <span class="token comment">//可选</span>
       <span class="token comment">//语句</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>switch case 执行时，一定会先进行匹配，匹配成功返回当前 case 的值，再根据是否有 break，判断是否继续输出，或是跳出判断。<br> 如果 case 语句块中没有 break 语句时，JVM 并不会顺序输出每一个 case 对应的返回值，而是继续匹配，匹配不成功则返回默认 case。</p>`,14),c=[l];function t(p,o){return n(),a("div",null,c)}const u=s(i,[["render",t],["__file","10条件语句.html.vue"]]);export{u as default};
