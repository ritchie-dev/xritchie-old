const moment = require('moment');

module.exports = {
  themeConfig: {
    search: true,
    // 调整搜索框显示的搜索结果数量：
    searchMaxSuggestions: 7,
    
  },
  
  plugins: [
    [
      '@vuepress/last-updated',
      {
        transformer: (timestamp, lang) => {
          // 不要忘了安装 moment
          // const moment = require('moment')
          moment.locale(lang)
          return moment(timestamp).format('lll')
        }
      }
    ]
  ],
  
}



