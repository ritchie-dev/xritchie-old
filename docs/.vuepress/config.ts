// import { defineUserConfig } from 'vuepress'
import { defaultTheme, defineUserConfig } from 'vuepress'
import { docsearchPlugin } from '@vuepress/plugin-docsearch'



export default defineUserConfig({
  
  // base: '/页面路径/',
  lang: 'zh-CN',
  title: 'Ritchie\'s Blog',
  description: '这是我的第一个 VuePress 站点',
  head: [
    // 设置 favor.ico，.vuepress/public 下
    [
      'link', { rel: 'icon', href: '/images/favicon.ico' }
    ],
  ],
  // locales: {
  //   // 键名是该语言所属的子路径
  //   // 作为特例，默认语言可以使用 '/' 作为其路径。
  //   '/zh/': {
  //     lang: 'zh-CN',
  //     title: '中文页面Title',
  //     description: 'Vue 驱动的静态网站生成器',
  //   },
  //   '/': {
  //     lang: 'en-US',
  //     title: '英文页面Title',
  //     description: 'Vue-powered Static Site Generator',
  //   },
    // contributors: true,
  // },
  
  theme: defaultTheme({
    
    // contributors: true,
    // contributorsText: '贡献者列表',
    // lastUpdated: true,
    // Public 文件路径
    logo: '/images/cool.jpg',
    
    // locales: {
    //   '/': {
    //     selectLanguageName: 'English',
    //   },
    //   '/zh/': {
    //     selectLanguageName: '简体中文',
    //   },
    // },
    // 默认主题配置
    navbar: [
      {
        text: '首页',
        link: '/',
      },
      // 嵌套 Java - 最大深度为 2
      {
        text: 'Java',
        activeMatch: '/java/java.md',
        children: [
          {
            text: '基础',
            children: ['/java/se/01入门.md', '/java/se/02环境配置.md', '/java/se/03对象.md'
            , '/java/se/04类.md', '/java/se/05数据类型.md', '/java/se/06变量.md'
            , '/java/se/07修饰符.md', '/java/se/08运算符.md', '/java/se/09循环语句.md'
            , '/java/se/10条件语句.md', '/java/se/11常用类.md', '/java/se/12正则表达式.md'
            , '/java/se/13数组.md', '/java/se/14方法.md', '/java/se/15异常.md'],
          },
          {
            text: '面向对象',
            children: ['/java/obj/01封装.md', '/java/obj/02继承.md', '/java/obj/03多态.md'
            , '/java/obj/04抽象类.md', '/java/obj/05接口.md', '/java/obj/06枚举和注解.md'],
          },
          {
            text: '中级',
            children: ['/java/sup/01数据结构.md', '/java/sup/02网络编程.md', '/java/sup/03java8新特性.md'
            , '/java/sup/04io.md', '/java/sup/05集合.md', '/java/sup/06泛型.md'
            , '/java/sup/07lambda.md', '/java/sup/08多线程.md', '/java/sup/09反射.md'
            , '/java/sup/10定时器.md'],
          },  
        ],
      },
      {
        text: '前端',
        link: '/',
        children: [
          {
            text: 'HTML',
            link: '/web/01html.md',
          },
          {
            text: 'CSS',
            link: '/web/02css.md',
          },
          {
            text: 'JavaScript',
            link: '/web/03js.md',
          },
          {
            text: 'JSON',
            link: '/web/04json.md',
          },
          {
            text: 'Ajax',
            link: '/web/05ajax.md',
          },
          {
            text: 'jQuery',
            link: '/web/06jquery.md',
          },
          {
            text: 'BootStrap',
            link: '/web/07bootstrap.md',
          },
          {
            text: 'Vue',
            link: '/web/08vue.md',
          },
          {
            text: 'ECharts',
            link: '/web/09echarts.md',
          },
          {
            text: 'ElementUI',
            link: '/web/10ele-ui.md',
          },
          {
            text: 'Node.js',
            link: '/web/11nodejs.md',
          },{
            text: 'Axios',
            link: '/web/12axios.md',
          },{
            text: 'Less',
            link: '/web/13less.md',
          },
        ]
      },
      {
        text: 'JavaEE',
        children: [
          {
            text: 'JDBC',
            link: '/javaee/01jdbc.md',
          },
          {
            text: 'Tomcat',
            link: '/javaee/02tomcat.md',
          },
          {
            text: 'Servlet',
            link: '/javaee/03servlet.md',
          },
          {
            text: 'JSP',
            link: '/javaee/04jsp.md',
          },
          {
            text: 'MySQL',
            link: '/javaee/05mysql.md',
          },
          
        ]
      },
      // 控制元素何时被激活
      {
        text: 'Java框架',
        children: [
          {
            text: 'MyBatis',
            link: '/frame/mybatis.md'
          },
          {
            text: 'Spring',
            link: '/frame/spring.md',
            // 该元素将一直处于激活状态
            // activeMatch: '/',
          },
          {
            text: 'SpringMVC',
            link: '/frame/springmvc.md',
          },
          {
            text: 'Spring Boot',
            link: '/frame/foo/spring-boot.md',
            // 该元素在当前路由路径是 /foo/ 开头时激活
            // 支持正则表达式
            activeMatch: '^/foo/',
          },
          {
            text: 'Shiro',
            link: '/frame/foo/shiro.md',
            activeMatch: '^/foo/',
          },
          {
            text: 'Spring Security',
            link: '/frame/foo/spring-security.md',
            activeMatch: '^/foo/',
          },
          {
            text: 'Spring Cloud(一)',
            link: '/frame/nofoo/spring-cloud1.md',
            activeMatch: '^/foo/',
          },
          {
            text: 'Spring Cloud(二)',
            link: '/frame/nofoo/spring-cloud2.md',
            activeMatch: '^/foo/',
          }
        ],
      },
      {
        text: 'DevTools',
        children: [
          {
            text: 'Git',
            link: '/tools/git.md',
          },
          {
            text: 'Maven',
            link: '/tools/maven.md',
          },
          {
            text: 'Nginx',
            link: '/tools/nginx.md',
          },
          {
            text: 'Redis',
            link: '/tools/redis.md',
          },{
            text: 'Linux',
            link: '/tools/linux.md',
          },
          {
            text: 'Docker',
            link: '/tools/docker.md',
          },
        ],
      },
      {
        text: 'FAQ',
        children: [
          {
            text: '运算相关',
            children: [
              '/faq/sub/qa1.md', '/faq/sub/qa2.md',
            ],
          },
          {
            text: '一些问题',
            children: [
              '/faq/interview/01线程.md', '/faq/interview/02Nginx.md', '/faq/interview/03数据库.md', 
              '/faq/interview/04Spring.md', '/faq/interview/05Maven.md', '/faq/interview/06集合.md', 
              '/faq/interview/07SpringMVC.md', '/faq/interview/08类.md', '/faq/interview/09MySQL.md',
              '/faq/interview/10Servlet.md', '/faq/interview/11SpringBoot.md', '/faq/interview/12Git.md', 
              '/faq/interview/13MyBatis.md', '/faq/interview/14Linux+Docker.md', '/faq/interview/15JVM.md', 
              '/faq/interview/16Redis.md', 
            ],
          },

          {
            text: '其他',
            children: [
              '/faq/other/空对象模式.md', 
            ],
          },
          
        ]
      },
      {
        text: '关于',
        link: '/about',
       
      }
    ],
    // 根据页面标题自动生成侧边栏
    // sidebar: 'auto'
    // sidebar: [
    //   // 这里展示的是对应页面的title，名字要跟上面的text对应
    //   // 这里要注意层级关系
    //   '',
    //   'about',
    //   'PUBG',

    // ]
  }),

  plugins: [
    docsearchPlugin({
      // 配置项
      appId: '',
      apiKey: '',
      indexName: ''
    }),
  ],

  

})
