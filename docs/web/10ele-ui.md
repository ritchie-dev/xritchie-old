---
lang: zh-CN
title: ElementUI
description: web 10
---

## Elment UI
基于Vue 2.x，[官方文档](https://element.eleme.cn/#/zh-CN)
## Element+
基于Vue 3.x，[官方文档](https://element-plus.gitee.io/zh-CN/guide/quickstart.html)

### Nuxt.js
[Nuxt.js](https://nuxt.com/docs/getting-started/introduction)是一个基于Vue.js的通用应用框架。主要关注的是应用的UI渲染。

Nuxt.js预设了利用Vue.js开发服务端渲染的应用所需要的各种配置。


#### 为什么用Nuxt.js？
Vue.js原来是开发 SPA(单页应用) 的，现在很多人想用Vue开发多页应用，并想要在服务端完成渲染。Nuxt.js应运而生，她简化了 SSR(服务端渲染) 的开发难度，而且还可以直接用命令把我们制作的vue项目生成静态html。

SPA不利于SEO搜索引擎优化（Search Engine Optimization）操作，