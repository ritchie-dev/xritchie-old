---
lang: zh-CN
title: Vue
description: web 08
---

## 什么是 Vue？
>Vue (发音为 /vjuː/，类似 view) 是一款用于构建用户界面的 JavaScript 框架。它基于标准 HTML、CSS 和 JavaScript 构建，并提供了一套声明式的、组件化的编程模型，帮助你高效地开发用户界面。无论是简单还是复杂的界面，Vue 都可以胜任。

[Vue 3官方文档](https://cn.vuejs.org/guide/introduction.html)

## 什么是Vue Router？
>Vue Router 是 Vue.js 的官方路由。它与 Vue.js 核心深度集成，让用 Vue.js 构建单页应用变得轻而易举。功能包括：

- 嵌套路由映射
- 动态路由选择
- 模块化、基于组件的路由配置
- 路由参数、查询、通配符
- 展示由 Vue.js 的过渡系统提供的过渡效果
- 细致的导航控制
- 自动激活 CSS 类的链接
- HTML5 history 模式或 hash 模式
- 可定制的滚动行为
- URL 的正确编码

[Vue Router官方文档](https://router.vuejs.org/zh/introduction.html)


## 什么是Vite？
>Vite（法语意为 "快速的"，发音 /vit/，发音同 "veet"）是一种新型前端构建工具，能够显著提升前端开发体验。它主要由两部分组成：

- 一个开发服务器，它基于 原生 ES 模块 提供了 丰富的内建功能，如速度快到惊人的 模块热更新（HMR）。

- 一套构建指令，它使用 Rollup 打包你的代码，并且它是预配置的，可输出用于生产环境的高度优化过的静态资源。

[Vite官方文档](https://cn.vitejs.dev/guide/)

## 什么是Vuex？
>Vuex 是一个专为 Vue.js 应用程序开发的状态管理模式 + 库。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。

>Vuex 3.x 只适配 Vue 2，而 Vuex 4.x 是适配 Vue 3 的。

[Vuex官方文档](https://vuex.vuejs.org/zh/)

## 什么是Pinia？
>Pinia (发音为 /piːnjʌ/，类似英文中的 “peenya”) 是 Vue 的专属状态管理库，它允许你跨组件或页面共享状态。  
与 Vuex 相比，Pinia 不仅提供了一个更简单的 API，也提供了符合组合式 API 风格的 API，最重要的是，搭配 TypeScript 一起使用时有非常可靠的类型推断支持。

[Pinia官方文档](https://pinia.vuejs.org/zh/introduction.html)

**Volar插件**
VSCode上的[Vue Language Features(Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.volar)插件