---
# 如果不需要导航栏
# navbar: false
# 这是浏览器标签页上方左边和页面坐标顶部展示的信息
title: About Me
# 当前页面禁用搜索框
search: false
# 当前页禁用侧边栏
sidebar: false
---


<center><h2>关于我</h2></center>



#### 一位终身学习者，热爱编程，喜欢探索新技术，平日喜欢玩游戏和弹吉他，享受独自旅行！
#### 这是我的[GitHub](https://github.com/wzy-ritchie) ，你可以和我一起交流技术，探讨经验或者对文档提出建议。


