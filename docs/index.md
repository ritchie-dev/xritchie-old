---
home: true
heroImage: /images/snow.png
heroText: 王则钰的博客
tagline: Ritchie's Blog
actionText: 快速上手 →
actionLink: /zh/guide/
features:

- title: 保持简单
  details: Keep it Simple, Stupid
- title: 自律坚持
  details: Life is just a moment...    Live it,  Love it,  Enjoy it
- title: 爱与自由
  details: You are still alive. Act like it.
# footer: MIT Licensed | Copyright © 2018-present Evan You
footerHtml: true
footer: Powered by <a href="https://vuepress.vuejs.org/zh/">VuePress</a> | Copyright © 2022-present 王则钰
---

### :sparkles:建议与交流
在阅读过程中有任何问题和想法，可以微信联系我：  
<img src="/images/wx.jpg" width = "100" alt="图片名称" align=center />