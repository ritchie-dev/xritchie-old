---
lang: zh-CN
title: ShiroSecurity
description: interview ShiroSecurity 17
---

## 1、Shiro和Security的区别?
Shiro 主要分为两个部分就是认证和授权。三个核心组件：Subject, SecurityManager 和 Realms。Subject：即“当前操作用户”。Subject代表了当前用户的安全操作，SecurityManager则管理所有用户的安全操作。

SecurityManager：它是Shiro框架的核心。
Realm：Realm充当了Shiro与应用安全数据间的“桥梁”或者“连接器”。Realm实质上是一个安全相关的DAO：它封装了数据源的连接细节，并在需要时将相关数据提供给Shiro。

Spring Security提供有若干个过滤器，它们能够拦截Servlet请求，

Spring Security是一个重量级的安全管理框架；Shiro则是一个轻量级的安全管理框架
Spring Security 功能比Shiro更加丰富些，例如安全维护方面；
Shiro 的配置和使用比较简单，Spring Security 上手复杂些；
Shiro 依赖性低，不需要任何框架和容器，可以独立运行， Spring Security依赖Spring容器；
Spring Security 自带了密码加盐。

## 2、RBAC权限模型?
RBAC模型（Role-Based Access Control：基于角色的访问控制）是比较早期提出的权限实现模型。Who是否可以对What进行How的访问操作，
Who、What、How构成了访问权限三元组，在RBAC模型里面，有3个基础组成部分，分别是：用户、角色和权限。
User（用户）：每个用户都有唯一的UID识别，并被授予不同的角色
Role（角色）：不同角色具有不同的权限
Permission（权限）：访问权限
用户-角色映射：用户和角色之间的映射关系
角色-权限映射：角色和权限之间的映射