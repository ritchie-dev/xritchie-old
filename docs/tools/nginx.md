---
lang: zh-CN
title: Nginx
description: tools 03
---




web应用服务器:
- Tomcat
- Jetty
- Undertow
web服务器:
- Apache服务器
- Nginx反向代理

区别：
web服务器不能解析jsp等页面，只能处理html、css、js等静态资源，但是它的并发能力远远高于web应用服务器。
